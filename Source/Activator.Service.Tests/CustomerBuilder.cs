﻿using System;
using System.Linq;
using Customer.API.Contracts;
using Customer.API.Contracts.Builders;
using Intelligent.Core.Models;

namespace Activator.Service.Tests
{
    internal static class CustomerBuilder
    {
        internal static CustomerContract GetCustomer(string languageCode)
        {
            var primaryContact = new ContactContractBuilder()
                    .WithFirstName("Zeno")
                    .WithLastName("Dierick")
                    .WithTitle("Dhr.")
                    .WithEmailAddress("dev-testing-test@intelli.gent")
                    .WithPhone(new PhoneContract("+32.56111111", PhoneTypeContract.Regular))
                    .WithContactType(ContactTypeContract.Primary)
                    .WithLanguage(languageCode)
                    .WithPriority(1)
                    .WithFunctionId(1)
                    .Build();

            var invoicingAddress = new AddressContractBuilder()
                    .WithStreet("Skaldenstraat")
                    .WithNumber("121")
                    .WithPostalCode("9042")
                    .WithCity("Gent")
                    .WithCountryCode("BE")
                    .WithAddressType(AddressTypeContract.Invoicing)
                    .Build();

            var provider = new Provider(ProviderValue.Combell);

            return new CustomerContractBuilder()
                   .WithCustomerNumber(129829)
                   .WithLanguage("nl")
                   .WithContact(primaryContact)
                   .WithAddress(invoicingAddress)
                   .WithProvider(provider.ProviderValue)
                   .withCustomerType(CustomerTypeContract.RegularCustomer)
                   .Build();
        }
    }
}
