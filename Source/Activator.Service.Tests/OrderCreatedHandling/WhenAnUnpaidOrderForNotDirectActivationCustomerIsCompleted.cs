﻿using Activator.Messages.Commands;
using Activator.Service.EventHandlers;
using Billing.API.Common.Interface.Contracts.Data;
using Intelligent.Core.Models;
using Intelligent.Shared.Testing.NUnit;
using MassTransit;
using NUnit.Framework;
using Rhino.Mocks;
using Serilog;
using Store.Messages.Events;
using Store.Messages.Payment;
using System;
using System.Threading;
using System.Threading.Tasks;
using Activator.Proxies;

namespace Activator.Service.Tests.OrderCreatedHandling
{
    [TestFixture]
    public class WhenAnUnpaidOrderForNotDirectActivationCustomerIsCompleted : GivenWhenThen
    {
        private OrderCreatedHandler _orderCreatedHandler;
        private ICustomerServiceProxy _customerServiceProxy;

        private string _orderCode;
        private ConsumeContext<OrderCreated> _consumeContext;

        protected override void Given()
        {
            _customerServiceProxy = MockRepository.GenerateMock<ICustomerServiceProxy>();
            var logger = MockRepository.GenerateMock<ILogger>();
            _orderCreatedHandler = new OrderCreatedHandler(_customerServiceProxy, logger);

            _consumeContext = MockRepository.GenerateMock<ConsumeContext<OrderCreated>>();
            _orderCode = "EASdf5df5d5d";
            var customerId = 129829;
            _consumeContext.Expect(x => x.Message).Return(new OrderCreated(_orderCode, CheckoutType.BankTransfer, customerId, "salesreference", DateTime.Now)).Repeat.AtLeastOnce();
            _consumeContext.Expect(x => x.Publish(Arg<ActivateOrder>.Is.Anything, Arg<CancellationToken>.Is.Anything)).Repeat.Never();

            _customerServiceProxy.
                Expect(x => x.GetCustomerInvoiceCriteria(Arg<CustomerIdentifier>.Matches(m => m.CustomerId == customerId))).
                Return(new CustomerInvoiceCriteriaDataContract() { DirectActivation = false }).
                Repeat.Once();
        }

        protected override void When()
        {
            _orderCreatedHandler.Consume(_consumeContext).Wait();
        }

        [Test]
        public void ShouldNotActivateOrder()
        {
            _consumeContext.VerifyAllExpectations();
            _customerServiceProxy.VerifyAllExpectations();
        }
    }
}