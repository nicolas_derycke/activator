﻿using Activator.Messages.Commands;
using Activator.Service.EventHandlers;
using Billing.Messages;
using Intelligent.Shared.Testing.NUnit;
using MassTransit;
using NUnit.Framework;
using Rhino.Mocks;
using Serilog;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Activator.Service.Tests.InvoiceFullyPaidHandling
{
    [TestFixture]
    public class WhenAnOrderIsPaid : GivenWhenThen
    {
        private OrderPaidHandler _orderPaidHandler;
        private string _orderCode;
        private ConsumeContext<OrderPaid> _consumeContext;

        protected override void Given()
        {
            var logger = MockRepository.GenerateMock<ILogger>();
            _consumeContext = MockRepository.GenerateMock<ConsumeContext<OrderPaid>>();

            _orderCode = "COM-87fdd5d";
            _consumeContext.Expect(x => x.Message).Return(new OrderPaid(_orderCode, DateTime.Now)).Repeat.AtLeastOnce();
            _consumeContext.Expect(x => x.Publish(Arg<ActivateOrder>.Matches(ao => ao.OrderCode == _orderCode), Arg<CancellationToken>.Is.Anything)).Return(Task.FromResult(0)).Repeat.Once();

            _orderPaidHandler = new OrderPaidHandler(logger);
        }

        protected override void When()
        {
            _orderPaidHandler.Consume(_consumeContext).Wait();
        }

        [Test]
        public void ShouldActivateOrder()
        {
            _consumeContext.VerifyAllExpectations();
        }
    }
}
