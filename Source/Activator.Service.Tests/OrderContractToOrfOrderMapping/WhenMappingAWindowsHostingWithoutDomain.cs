﻿using Combell.Model.OrderForm;
using FluentAssertions;
using NUnit.Framework;
using Store.API.Contracts;
using Store.API.Contracts.Orders;
using Store.API.Contracts.TypedItemAttributes;
using System.Collections.Generic;
using System.Linq;
using Activator.Services;
using Combell.Model.UAC;
using Combell.Model.UAC.ResourceSets;
using Rhino.Mocks;

namespace Activator.Service.Tests.OrderContractToOrfOrderMapping
{
    public class WhenMappingAWindowsHostingWithoutDomain : MappingTest
    {
        protected override void StubServicePackage(IServicePackageProvider servicePackageProvider)
        {
            servicePackageProvider
                .Stub(x => x.GetServicePackage(154, 0))
                .Return(new UAC_ServicePackage
                {
                    ResourceSets = new List<UAC_ResourceSet>
                    {
                        new UAC_HostingResourceSet
                        {
                            ResourceSets = new List<UAC_ResourceSet>
                            {
                                new UAC_WindowsHostingResourceSet()
                            }
                        }
                    },
                });
        }

        protected override IEnumerable<OrderItemContract> GetOrderItemsToMap()
        {
            yield return new OrderItemContract()
                        {
                            Attributes = new ItemAttributeDictionary()
                            {
                                {AttributeKey.Domain, new DomainItemAttributes("zeno.be")}
                            },
                            ProductId = 154,
                            PeriodId = 1,
                            Price = new PriceContract()
                            {
                                ExclVat = 75,
                                ReductionExclVat = 25
                            },
                            Period = 6,
                            ProductCode = "windows-express",
                            ProductType = "windows-shared-hosting",
                            ProductName = "Windows express",
                            Quantity = 1
                        };
        }

        [Test]
        public void ShouldOrfOrderContainWindowsData()
        {
            MappedOrder.Should().NotBeNull();
            MappedOrder.HostingPackages.Should().NotBeNull();
            MappedOrder.HostingPackages.Should().HaveCount(1);

            var hostingPackage = MappedOrder.HostingPackages.First();
            hostingPackage.ID.Should().Be(154);
            hostingPackage.PeriodID.Should().Be(1);
            hostingPackage.Period.Should().Be(6);
            hostingPackage.Price.Should().Be(75);
            hostingPackage.StandardPrice.Should().Be(100);
            hostingPackage.DiscountPercentage.Should().Be(0.25m);
            hostingPackage.Name.Should().Be("Windows express");

            hostingPackage.Domains.Should().NotBeNull();
            hostingPackage.Domains.Should().HaveCount(1);

            var domain = hostingPackage.Domains.First();
            domain.DomainName.Should().Be("zeno.be");
            domain.NameServerAction.Should().Be(NameServerActionTypes.Forwarding);
            domain.IsMainDomain.Should().BeTrue();
            domain.DomainAction.Should().Be(DomainActionTypes.NoAction);
        }
    }
}