﻿using Combell.Model.OrderForm;
using FluentAssertions;
using NUnit.Framework;
using Store.API.Contracts;
using Store.API.Contracts.Orders;
using Store.API.Contracts.TypedItemAttributes;
using System.Collections.Generic;
using System.Linq;
using Activator.Services;
using Combell.Model.UAC;
using Combell.Model.UAC.Enumerations;
using Combell.Model.UAC.ResourceSets;
using Rhino.Mocks;

namespace Activator.Service.Tests.OrderContractToOrfOrderMapping
{
    public class WhenMappingAMemcachedAddon : MappingTest
    {
        protected override void StubServicePackage(IServicePackageProvider servicePackageProvider)
        {
            servicePackageProvider
           .Stub(x => x.GetServicePackage(154, 0))
           .Return(new UAC_ServicePackage
           {
               ResourceSets = new List<UAC_ResourceSet>
               {
                        new UAC_HostingResourceSet
                        {
                            ResourceSets = new List<UAC_ResourceSet>
                            {
                                new UAC_LinuxHostingResourceSet
                                {
                                }
                            }
                        }
               },
               Type = UAC_ServicePackageType.AddOn
           });
        }

        protected override IEnumerable<OrderItemContract> GetOrderItemsToMap()
        {
            yield return new OrderItemContract()
                        {
                            Attributes = new ItemAttributeDictionary()
                            {
                                {AttributeKey.Domain, new DomainItemAttributes("zeno.be")},
                                {AttributeKey.UacAccountAddon, new UacAccountAddonAttributes(987, null)}
                            },
                            ProductId = 154,
                            PeriodId = 5,
                            Price = new PriceContract()
                            {
                                ExclVat = 75,
                                ReductionExclVat = 25
                            },
                            Period = 12,
                            ProductCode = "caching_memcached_64MB_addon",
                            ProductType = "caching",
                            ProductName = "Memcached 64 MB",
                            Quantity = 1
                        };
        }

        [Test]
        public void ShouldOrfOrderContainUacAccountAddonData()
        {
            MappedOrder.Should().NotBeNull();
            MappedOrder.UacAccountAddons.Should().NotBeNull();
            MappedOrder.UacAccountAddons.Should().HaveCount(1);

            var uacAccountAddon = MappedOrder.UacAccountAddons.First();
            uacAccountAddon.ID.Should().Be(154);
            uacAccountAddon.PeriodID.Should().Be(5);
            uacAccountAddon.Period.Should().Be(12);
            uacAccountAddon.Price.Should().Be(75);
            uacAccountAddon.StandardPrice.Should().Be(100);
            uacAccountAddon.DiscountPercentage.Should().Be(0.25m);
            uacAccountAddon.Identifier.Should().Be("zeno.be");
            uacAccountAddon.AccountId.Should().Be(987);
            uacAccountAddon.Name.Should().Be("Memcached 64 MB");
        }
    }
}
