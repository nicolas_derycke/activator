﻿using Combell.Model.OrderForm;
using FluentAssertions;
using NUnit.Framework;
using Store.API.Contracts;
using Store.API.Contracts.Orders;
using Store.API.Contracts.TypedItemAttributes;
using System.Collections.Generic;
using System.Linq;
using Activator.Services;
using Combell.Model.UAC;
using Combell.Model.UAC.ResourceSets;
using Intelligent.Shared.Core;
using Rhino.Mocks;

namespace Activator.Service.Tests.OrderContractToOrfOrderMapping
{
    public class WhenMappingAMultiDomainCertificate : MappingTest
    {
        protected override void StubServicePackage(IServicePackageProvider servicePackageProvider)
        {
            servicePackageProvider
                .Stub(x => x.GetServicePackage(1, 0))
                .Return(new UAC_ServicePackage
                {
                    ResourceSets = new List<UAC_ResourceSet>
                    {
                        new UAC_HostingResourceSet
                        {
                            ResourceSets = new List<UAC_ResourceSet>
                            {
                                new UAC_SSLResourceSet()
                            }
                        }
                    }
                });

            servicePackageProvider
                .Stub(x => x.GetServicePackage(3, 0))
                .Return(new UAC_ServicePackage
                {
                    ResourceSets = new List<UAC_ResourceSet>
                    {
                        new UAC_HostingResourceSet
                        {
                            ResourceSets = new List<UAC_ResourceSet>
                            {
                                new UAC_SSLResourceSet()
                            }
                        }
                    }
                });

            servicePackageProvider
                .Stub(x => x.GetServicePackage(5, 0))
                .Return(new UAC_ServicePackage
                {
                    ResourceSets = new List<UAC_ResourceSet>
                    {
                        new UAC_HostingResourceSet
                        {
                            ResourceSets = new List<UAC_ResourceSet>
                            {
                                new UAC_SSLResourceSet()
                            }
                        }
                    }
                });
        }

        protected override IEnumerable<OrderItemContract> GetOrderItemsToMap()
        {
            yield return new OrderItemContract()
            {
                Attributes = new ItemAttributeDictionary()
                {
                    {AttributeKey.Certificate, new CertificateAttributes(new List<string>(){"my-multi-domain-certificate.be"})}
                },
                ProductId = 1,
                PeriodId = 2,
                Price = new PriceContract()
                {
                    ExclVat = 3,
                    ReductionExclVat = 0
                },
                Period = 12,
                ProductCode = "comodo-positive-multi-domain-ssl-certificaat-promo",
                ProductType = "multi-domain-domain-validation",
                ProductName = "Comodo positive multi domain ssl certificaat",
                Quantity = 1,
                OrderItemId = 987
            };

            yield return new OrderItemContract()
            {
                Attributes = new ItemAttributeDictionary()
                            {
                                {AttributeKey.Certificate, new CertificateAttributes(new List<string>(){"extra-hostname-1.be"})}
                            },
                ProductId = 3,
                PeriodId = 4,
                Price = new PriceContract()
                {
                    ExclVat = 0,
                    ReductionExclVat = 0
                },
                Period = 12,
                ProductCode = "comodo-positive-multi-domain-ssl-certificaat-extra-free-hostname",
                ProductType = "options-ssl",
                ProductName = "Comodo positive multi domain ssl certificaat extra free hostname",
                Quantity = 1,
                ParentId = 987
            };

            yield return new OrderItemContract()
            {
                Attributes = new ItemAttributeDictionary()
                            {
                                {AttributeKey.Certificate, new CertificateAttributes(new List<string>(){"extra-hostname-2.be"})}
                            },
                ProductId = 5,
                PeriodId = 6,
                Price = new PriceContract()
                {
                    ExclVat = 7,
                    ReductionExclVat = 0
                },
                Period = 12,
                ProductCode = "comodo-positive-multi-domain-ssl-certificaat-extra-hostname",
                ProductType = "options-ssl",
                ProductName = "Comodo positive multi domain ssl certificaat extra hostname",
                Quantity = 1,
                ParentId = 987
            };
        }

        [Test]
        public void ShouldOrfOrderContainCertificateData()
        {
            MappedOrder.Should().NotBeNull();
            MappedOrder.SSLCertificates.Should().NotBeNull();
            MappedOrder.SSLCertificates.Should().HaveCount(1);

            var sslCertificate = MappedOrder.SSLCertificates.First();
            sslCertificate.ID.Should().Be(1);
            sslCertificate.PeriodID.Should().Be(2);
            sslCertificate.Period.Should().Be(12);
            sslCertificate.Price.Should().Be(3);
            sslCertificate.StandardPrice.Should().Be(3);
            sslCertificate.DiscountPercentage.Should().Be(0);
            sslCertificate.MainHostname.Should().Be("my-multi-domain-certificate.be");
            sslCertificate.Name.Should().Be("Comodo positive multi domain ssl certificaat");

            sslCertificate.MultiDomainHostnames.Should().HaveCount(2);

            var firstExtraHostName = sslCertificate.MultiDomainHostnames.First();
            firstExtraHostName.ID.Should().Be(3);
            firstExtraHostName.PeriodID.Should().Be(4);
            firstExtraHostName.Period.Should().Be(12);
            firstExtraHostName.Price.Should().Be(0);
            firstExtraHostName.StandardPrice.Should().Be(0);
            firstExtraHostName.DiscountPercentage.Should().Be(0);
            firstExtraHostName.Hostname.Should().Be("extra-hostname-1.be");
            firstExtraHostName.Name.Should().Be("Comodo positive multi domain ssl certificaat extra free hostname");

            var secondExtraHostName = sslCertificate.MultiDomainHostnames[1];
            secondExtraHostName.ID.Should().Be(5);
            secondExtraHostName.PeriodID.Should().Be(6);
            secondExtraHostName.Period.Should().Be(12);
            secondExtraHostName.Price.Should().Be(7);
            secondExtraHostName.StandardPrice.Should().Be(7);
            secondExtraHostName.DiscountPercentage.Should().Be(0);
            secondExtraHostName.Hostname.Should().Be("extra-hostname-2.be");
            secondExtraHostName.Name.Should().Be("Comodo positive multi domain ssl certificaat extra hostname");
        }
    }
}
