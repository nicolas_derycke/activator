﻿using Combell.Model.OrderForm;
using FluentAssertions;
using NUnit.Framework;
using Store.API.Contracts;
using Store.API.Contracts.Orders;
using Store.API.Contracts.TypedItemAttributes;
using System.Collections.Generic;
using System.Linq;

namespace Activator.Service.Tests.OrderContractToOrfOrderMapping
{
    public class WhenMappingALinuxUniqueIp : MappingTest
    {
        protected override IEnumerable<OrderItemContract> GetOrderItemsToMap()
        {
            yield return new OrderItemContract()
                        {
                            Attributes = new ItemAttributeDictionary()
                            {
                                {AttributeKey.Domain, new DomainItemAttributes("zeno.be")}
                            },
                            ProductId = 154,
                            PeriodId = 5,
                            Price = new PriceContract()
                            {
                                ExclVat = 25,
                                ReductionExclVat = 25
                            },
                            Period = 12,
                            ProductCode = "uniek-ip-adres-voor-linux-hosting",
                            ProductType = "uniek-ip-adres",
                            ProductName = "Unique IP address for Linux hosting",
                            Quantity = 1
                        };
        }

        [Test]
        public void ShouldOrfOrderContainLinuxData()
        {
            MappedOrder.Should().NotBeNull();
            MappedOrder.UniqueIPs.Should().NotBeNull();
            MappedOrder.UniqueIPs.Should().HaveCount(1);

            var uniqueIp = MappedOrder.UniqueIPs.First();
            uniqueIp.ID.Should().Be(154);
            uniqueIp.PeriodID.Should().Be(5);
            uniqueIp.Period.Should().Be(12);
            uniqueIp.Price.Should().Be(25);
            uniqueIp.StandardPrice.Should().Be(50);
            uniqueIp.DiscountPercentage.Should().Be(0.5m);
            uniqueIp.Name.Should().Be("Unique IP address for Linux hosting");
            uniqueIp.Identifier.Should().Be("zeno.be");
        }
    }
}
