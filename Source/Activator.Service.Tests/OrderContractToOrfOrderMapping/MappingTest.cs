﻿using Billing.API.Common.Interface.Contracts.Data;
using Combell.Model.OrderForm;
using Customer.API.Contracts;
using FluentAssertions;
using Intelligent.Core.Mappers;
using Intelligent.Core.Models;
using Intelligent.Shared.Testing.NUnit;
using NUnit.Framework;
using Rhino.Mocks;
using Store.API.Contracts.Orders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Activator.Proxies;
using Activator.Services;
using Activator.Services.Mapping;
using Combell.Model.OrderQueue;
using Combell.Model.UAC;
using Intelligent.Shared.Core;
using Serilog;

namespace Activator.Service.Tests.OrderContractToOrfOrderMapping
{
    [TestFixture]
    public abstract class MappingTest : GivenWhenThen
    {
        private ICustomerApiProxy _customerApiProxy;
        private ICustomerServiceProxy _customerServiceProxy;

        private IOrderContractToOrfOrderMapper _mapper;

        private OrderContract _orderToMap;
        protected ORF_Order MappedOrder;
        private ORF_Product _product;

        protected CustomerContract Customer;
        protected ContactContract PrimaryContact;
        protected AddressContract InvoicingAddress;
        private IServicePackageProvider _servicePackageProvider;

        private string _orderCode;
        private Provider _provider;
        private string _language;

        protected override void Given()
        {
            _customerApiProxy = MockRepository.GenerateMock<ICustomerApiProxy>();
            _servicePackageProvider = MockRepository.GenerateMock<IServicePackageProvider>();
            StubServicePackage(_servicePackageProvider);

            LanguageToStringMapper.TryMapLanguage(Language.Dutch, out _language);
            Customer = CustomerBuilder.GetCustomer(_language);
            _provider = new Provider((ProviderValue)Enum.Parse(typeof(ProviderValue), Customer.Provider, true));
            InvoicingAddress = Customer.Addresses.First();
            PrimaryContact = Customer.Contacts.First();

            _customerApiProxy.
                Expect(x => x.Get(Arg<CustomerIdentifier>.Matches(ci => ci.CustomerNumber == Customer.CustomerNumber))).
                Return(Task.FromResult(Customer)).
                Repeat.Once();

            _orderCode = "COM98562s5z";
            var orderItemsToMap = GetOrderItemsToMap();

            _orderToMap = new OrderContract()
            {
                CustomerNumber = Customer.CustomerNumber,
                OrderCode = _orderCode,
                VatAmount = 21,
                Items = orderItemsToMap,
                TicketId = 1111,
                TicketNumber = "Ticketnumber"
            };

            _product = new ORF_Product()
            {
                Id = 154,
                Name = "A product",
                Periods = new List<ORF_Period>()
                    {
                        new ORF_Period()
                        {
                            Id = 1,
                            DiscountPercentage = 2,
                            Months = 6,
                            Price = 3,
                            StandardPrice = 4
                        },
                        new ORF_Period()
                        {
                            Id = 5,
                            DiscountPercentage = 6,
                            Months = 12,
                            Price = 7,
                            StandardPrice = 8
                        },
                        new ORF_Period()
                        {
                            Id = 9,
                            DiscountPercentage = 10,
                            Months = 24,
                            Price = 11,
                            StandardPrice = 12
                        }
                    }
            };

            _customerServiceProxy = MockRepository.GenerateMock<ICustomerServiceProxy>();
            _customerServiceProxy.
                Expect(x => x.GetCustomerInvoiceCriteria(Arg<CustomerIdentifier>.Matches(ci => ci.CustomerNumber == Customer.CustomerNumber))).
                Return(new CustomerInvoiceCriteriaDataContract() { DirectActivation = true }).
                Repeat.Once();

            _mapper = new OrderContractToOrfOrderMapper(
                _customerApiProxy, 
                _customerServiceProxy, 
                new List<IOrderItemToProvisioningOrderMapper> 
                {
                    new CertificateMapper(), 
                    new CmsHostingPackageMapper(), 
                    new DomainMapper(), 
                    new EmailPackageMapper(), 
                    new HostingPackageMapper(), 
                    new UniqueIpMapper(), 
                    new UacAccountAddonMapper(),
                    new FaxPackageMapper()
                },
                _servicePackageProvider,
                new ManualActivationMapper(), 
                MockRepository.GenerateMock<ILogger>());
        }

        abstract protected IEnumerable<OrderItemContract> GetOrderItemsToMap();
        protected virtual void StubServicePackage(IServicePackageProvider servicePackageProvider){}

        protected override void When()
        {
            MappedOrder = _mapper.Map(CustomerIdentifier.FromCustomerNumber(_orderToMap.CustomerNumber), _orderToMap).Result;
        }

        [Test]
        public void ShouldOrfOrderContainOrderData()
        {
            MappedOrder.Should().NotBeNull();
            MappedOrder.OrderCode.Should().Be(_orderCode);
            MappedOrder.OrderOrigin.Should().Be(ORF_OrderOrigins.Store);
            MappedOrder.OrderPriority.Should().Be(OrderQueuePriority.High);
            MappedOrder.CreateInternalAccount.Should().BeFalse();
        }

        [Test]
        public void ShouldAllExpectationsBeMet()
        {
            _customerApiProxy.VerifyAllExpectations();
        }

        [Test]
        public void ShouldOrfOrderContainCustomerData()
        {
            MappedOrder.Should().NotBeNull();
            var orderCustomer = MappedOrder.OrderCustomer;
            orderCustomer.Should().NotBeNull();

            Language expectedLanguage;
            LanguageMapper.TryMapLanguage(_language, out expectedLanguage);

            orderCustomer.Company.Should().BeNullOrEmpty();
            orderCustomer.Country.Should().Be(InvoicingAddress.CountryCode);
            orderCustomer.CountryVATCode.Should().BeEmpty();
            orderCustomer.DeliveryCompany.Should().Be(_provider.Id);
            orderCustomer.EMail.Should().Be("dev-testing-test@intelli.gent");
            orderCustomer.Fax.Should().BeEmpty();
            orderCustomer.FirstName.Should().Be(PrimaryContact.FirstName);
            orderCustomer.Gsm.Should().BeEmpty();
            orderCustomer.HouseNumber.Should().Be(InvoicingAddress.Number);
            orderCustomer.ID.Should().Be(Customer.CustomerNumber - 7000);
            orderCustomer.IsReseller.Should().BeTrue();
            orderCustomer.LanguageId.Should().Be((int)expectedLanguage);
            orderCustomer.Name.Should().Be(PrimaryContact.LastName);
            orderCustomer.Place.Should().Be(InvoicingAddress.City);
            orderCustomer.PostalCode.Should().Be(InvoicingAddress.PostalCode);
            orderCustomer.Street.Should().Be(InvoicingAddress.Street);
            orderCustomer.TechContact.Should().BeEmpty();
            orderCustomer.TechContactFirstName.Should().BeEmpty();
            orderCustomer.TechContactLastName.Should().BeEmpty();
            orderCustomer.TechEmail.Should().BeEmpty();
            orderCustomer.TechGSM.Should().BeEmpty();
            orderCustomer.TechTel.Should().BeEmpty();
            orderCustomer.Telephone1.Should().Be("+32.56111111");
            orderCustomer.VATNumber.Should().BeEmpty();
        }

        [Test]
        public void ShouldOrfOrderContainTicketData()
        {
            MappedOrder.Ticket.TicketId.Should().Be(1111);
            MappedOrder.Ticket.TicketNumber.Should().Be("Ticketnumber");
        }
    }
}
