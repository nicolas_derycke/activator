﻿using Combell.Model.OrderForm;
using FluentAssertions;
using NUnit.Framework;
using Store.API.Contracts;
using Store.API.Contracts.Orders;
using Store.API.Contracts.TypedItemAttributes;
using System.Collections.Generic;
using System.Linq;
using Activator.Services;
using Combell.Model.UAC;
using Combell.Model.UAC.ResourceSets;
using Rhino.Mocks;

namespace Activator.Service.Tests.OrderContractToOrfOrderMapping
{
    public class WhenMappingADrupalHostingWithoutDomain : MappingTest
    {
        protected override void StubServicePackage(IServicePackageProvider servicePackageProvider)
        {
            servicePackageProvider
             .Stub(x => x.GetServicePackage(154, 0))
             .Return(new UAC_ServicePackage
             {
                 ResourceSets = new List<UAC_ResourceSet>
                 {
                        new UAC_HostingResourceSet
                        {
                            ResourceSets = new List<UAC_ResourceSet>
                            {
                                new UAC_LinuxHostingResourceSet
                                {
                                    Application = "Drupal"
                                }
                            }
                        }
                 }
             });
        }

        protected override IEnumerable<OrderItemContract> GetOrderItemsToMap()
        {
            yield return new OrderItemContract()
                        {
                            Attributes = new ItemAttributeDictionary()
                            {
                                {AttributeKey.Domain, new DomainItemAttributes("zeno.be")},
                                {AttributeKey.Cms, new CmsAttributes("v5.2", "en")}
                            },
                            ProductId = 154,
                            PeriodId = 5,
                            Price = new PriceContract()
                            {
                                ExclVat = 25,
                                ReductionExclVat = 25
                            },
                            Period = 12,
                            ProductCode = "drupal-express",
                            ProductType = "drupal",
                            ProductName = "Drupal express",
                            Quantity = 1
                        };
        }

        [Test]
        public void ShouldOrfOrderContainLinuxData()
        {
            MappedOrder.Should().NotBeNull();
            MappedOrder.HostingPackages.Should().NotBeNull();
            MappedOrder.HostingPackages.Should().HaveCount(1);

            var hostingPackage = MappedOrder.HostingPackages.First();
            hostingPackage.ID.Should().Be(154);
            hostingPackage.PeriodID.Should().Be(5);
            hostingPackage.Period.Should().Be(12);
            hostingPackage.Price.Should().Be(25);
            hostingPackage.StandardPrice.Should().Be(50);
            hostingPackage.DiscountPercentage.Should().Be(0.5m);
            hostingPackage.Name.Should().Be("Drupal express");
            hostingPackage.ApplicationVersion.Should().Be("v5.2");
            hostingPackage.ApplicationLanguage.Should().Be("en");

            hostingPackage.Domains.Should().NotBeNull();
            hostingPackage.Domains.Should().HaveCount(1);

            var domain = hostingPackage.Domains.First();
            domain.DomainName.Should().Be("zeno.be");
            domain.NameServerAction.Should().Be(NameServerActionTypes.Forwarding);
            domain.IsMainDomain.Should().BeTrue();
            domain.DomainAction.Should().Be(DomainActionTypes.NoAction);
        }
    }
}
