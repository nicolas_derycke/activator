﻿using Billing.API.Common.Interface.Contracts.Data;
using Combell.Model.OrderForm;
using Customer.API.Contracts;
using Customer.API.Contracts.Builders;
using FluentAssertions;
using Intelligent.Core.Mappers;
using Intelligent.Core.Models;
using Intelligent.Shared.Testing.NUnit;
using NUnit.Framework;
using Rhino.Mocks;
using Store.API.Contracts.Orders;
using System.Collections.Generic;
using System.Threading.Tasks;
using Activator.Proxies;
using Activator.Services;
using Activator.Services.Mapping;
using Serilog;

namespace Activator.Service.Tests.OrderContractToOrfOrderMapping
{
    [TestFixture]
    public class WhenMappingACustomerWithTechnicalContact : GivenWhenThen
    {
        private ICustomerApiProxy _customerApiProxy;
        private ICustomerServiceProxy _customerServiceProxy;

        private IOrderContractToOrfOrderMapper _mapper;

        private OrderContract _orderToMap;
        protected ORF_Order _mappedOrder;

        protected CustomerContract _customer;
        protected ContactContract _primaryContact;
        protected ContactContract _technicalContact;
        protected AddressContract _invoicingAddress;

        private Provider _provider;
        private string _language;
        private IServicePackageProvider _servicePackageProvider;

        protected override void Given()
        {
            _customerApiProxy = MockRepository.GenerateMock<ICustomerApiProxy>();
            _servicePackageProvider = MockRepository.GenerateMock<IServicePackageProvider>();

            LanguageToStringMapper.TryMapLanguage(Language.Dutch, out _language);

            _primaryContact = new ContactContractBuilder()
                    .WithFirstName("Zeno")
                    .WithLastName("Dierick")
                    .WithTitle("Dhr.")
                    .WithEmailAddress("dev-testing-test@intelli.gent")
                    .WithPhone(new PhoneContract("+32.56111111", PhoneTypeContract.Regular))
                    .WithPhone(new PhoneContract("fax", PhoneTypeContract.Fax))
                    .WithPhone(new PhoneContract("mobile", PhoneTypeContract.Mobile))
                    .WithContactType(ContactTypeContract.Primary)
                                                          .WithLanguage(_language)
                                                          .WithPriority(1)
                    .WithFunctionId(1)
                    .Build();

            _technicalContact = new ContactContractBuilder()
                    .WithFirstName("Liessa")
                    .WithLastName("Engels")
                    .WithTitle("Mevr.")
                    .WithEmailAddress("test@intelli.gent")
                    .WithPhone(new PhoneContract("+32.56111112", PhoneTypeContract.Regular))
                    .WithPhone(new PhoneContract("tech-gsm", PhoneTypeContract.Mobile))
                    .WithContactType(ContactTypeContract.Technical)
                    .WithLanguage("en")
                    .WithPriority(1)
                    .WithFunctionId(1)
                    .Build();

            _invoicingAddress = new AddressContractBuilder()
                    .WithStreet("Skaldenstraat")
                    .WithNumber("121")
                    .WithPostalCode("9042")
                    .WithCity("Gent")
                    .WithCountryCode("BE")
                    .WithAddressType(AddressTypeContract.Invoicing)
                    .Build();

            _provider = new Provider(ProviderValue.Combell);
            _customer = new CustomerContractBuilder()
                   .WithCustomerNumber(129829)
                   .WithLanguage("nl")
                   .WithContact(_primaryContact)
                   .WithContact(_technicalContact)
                   .WithAddress(_invoicingAddress)
                   .WithProvider(_provider.ProviderValue)
                   .withCustomerType(CustomerTypeContract.RegularCustomer)
                   .WithVatNumber(new VatNumberContract("vatnumber", "cz", false))
                   .Build();

            _customerApiProxy.
                Expect(x => x.Get(Arg<CustomerIdentifier>.Matches(ci => ci.CustomerNumber == _customer.CustomerNumber))).
                Return(Task.FromResult(_customer)).
                Repeat.Once();

            var orderCode = "COM98562s5z";
            _orderToMap = new OrderContract()
            {
                CustomerNumber = _customer.CustomerNumber,
                OrderCode = orderCode,
                VatAmount = 21,
                Items = new List<OrderItemContract>(),
                TicketId = 1111,
                TicketNumber = "Ticketnumber"
            };

            _customerServiceProxy = MockRepository.GenerateMock<ICustomerServiceProxy>();
            _customerServiceProxy.
                Expect(x => x.GetCustomerInvoiceCriteria(Arg<CustomerIdentifier>.Matches(ci => ci.CustomerNumber == _customer.CustomerNumber))).
                Return(new CustomerInvoiceCriteriaDataContract() { DirectActivation = false }).
                Repeat.Once();

            _mapper = new OrderContractToOrfOrderMapper(
                _customerApiProxy, 
                _customerServiceProxy,
                new List<IOrderItemToProvisioningOrderMapper>
                {
                    new CertificateMapper(),
                    new CmsHostingPackageMapper(),
                    new DomainMapper(),
                    new EmailPackageMapper(),
                    new HostingPackageMapper(),
                    new UniqueIpMapper(),
                    new UacAccountAddonMapper()
                },
                _servicePackageProvider,
                new ManualActivationMapper(), 
                MockRepository.GenerateMock<ILogger>());
        }

        protected override void When()
        {
            _mappedOrder = _mapper.Map(CustomerIdentifier.FromCustomerNumber(_orderToMap.CustomerNumber), _orderToMap).Result;
        }

        [Test]
        public void ShouldOrfOrderContainAllCustomerData()
        {
            _customerApiProxy.VerifyAllExpectations();

            _mappedOrder.Should().NotBeNull();
            var orderCustomer = _mappedOrder.OrderCustomer;
            orderCustomer.Should().NotBeNull();

            Language expectedLanguage;
            LanguageMapper.TryMapLanguage(_language, out expectedLanguage);

            orderCustomer.Company.Should().BeNullOrEmpty();
            orderCustomer.Country.Should().Be(_invoicingAddress.CountryCode);
            orderCustomer.CountryVATCode.Should().Be("cz");
            orderCustomer.DeliveryCompany.Should().Be(_provider.Id);
            orderCustomer.EMail.Should().Be("dev-testing-test@intelli.gent");
            orderCustomer.Fax.Should().Be("fax");
            orderCustomer.FirstName.Should().Be(_primaryContact.FirstName);
            orderCustomer.Gsm.Should().Be("mobile");
            orderCustomer.HouseNumber.Should().Be(_invoicingAddress.Number);
            orderCustomer.ID.Should().Be(_customer.CustomerNumber - 7000);
            orderCustomer.IsReseller.Should().BeFalse();
            orderCustomer.LanguageId.Should().Be((int)expectedLanguage);
            orderCustomer.Name.Should().Be(_primaryContact.LastName);
            orderCustomer.Place.Should().Be(_invoicingAddress.City);
            orderCustomer.PostalCode.Should().Be(_invoicingAddress.PostalCode);
            orderCustomer.Street.Should().Be(_invoicingAddress.Street);
            orderCustomer.TechContact.Should().Be("Liessa Engels");
            orderCustomer.TechContactFirstName.Should().Be("Liessa");
            orderCustomer.TechContactLastName.Should().Be("Engels");
            orderCustomer.TechEmail.Should().Be("test@intelli.gent");
            orderCustomer.TechGSM.Should().Be("tech-gsm");
            orderCustomer.TechTel.Should().Be("+32.56111112");
            orderCustomer.Telephone1.Should().Be("+32.56111111");
            orderCustomer.VATNumber.Should().Be("vatnumber");
        }

        [Test]
        public void ShouldOrfOrderContainTicketData()
        {
            _mappedOrder.Ticket.TicketId.Should().Be(1111);
            _mappedOrder.Ticket.TicketNumber.Should().Be("Ticketnumber");
        }
    }
}
