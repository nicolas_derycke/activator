using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using NUnit.Framework;
using Store.API.Contracts;
using Store.API.Contracts.Orders;

namespace Activator.Service.Tests.OrderContractToOrfOrderMapping
{
    public class WhenMappingProductWithoutAttributes : MappingTest
    {
        protected override IEnumerable<OrderItemContract> GetOrderItemsToMap()
        {
            yield return new OrderItemContract()
            {
                Attributes = new ItemAttributeDictionary(),
                ProductId = 154,
                PeriodId = 5,
                Price = new PriceContract()
                {
                    ExclVat = 25,
                    ReductionExclVat = 25
                },
                Period = 12,
                ProductCode = "unknown",
                ProductType = "unknown-group",
                ProductName = "Unknown",
                Quantity = 1
            };
        }

        [Test]
        public void OrfOrderContainsManualActivation()
        {
            MappedOrder.Should().NotBeNull();
            MappedOrder.ManualActivations.Should().HaveCount(1);

            var manualActivation = MappedOrder.ManualActivations.First();
            manualActivation.ID.Should().Be(154);
            manualActivation.PeriodID.Should().Be(5);
            manualActivation.Period.Should().Be(12);
            manualActivation.Price.Should().Be(25);
            manualActivation.StandardPrice.Should().Be(50);
            manualActivation.DiscountPercentage.Should().Be(0.5m);
            manualActivation.Name.Should().Be("Unknown");
            manualActivation.Quantity.Should().Be(1);
            manualActivation.ProductCode.Should().Be("unknown");

            manualActivation.Attributes.Should().BeEmpty();
        }
    }
}