﻿using System.Collections.Generic;
using System.Linq;
using Combell.Model.OrderForm;
using FluentAssertions;
using NUnit.Framework;
using Store.API.Contracts;
using Store.API.Contracts.Orders;
using Store.API.Contracts.TypedItemAttributes;

namespace Activator.Service.Tests.OrderContractToOrfOrderMapping
{
    public class WhenMappingATransferFaxOrder : MappingTest
    {
        protected override IEnumerable<OrderItemContract> GetOrderItemsToMap()
        {
            yield return new OrderItemContract
            {
                OrderItemId = 1,
                Period = 12,
                PeriodId = 1234,
                PeriodUnitType = PeriodUnitTypeContract.OneYear,
                Price = new PriceContract
                {
                    ExclVat = 100,
                    InclVat = 121,
                },
                ProductCode = "fax-services",
                ProductId = 6138,
                ProductName = "Fax Services",
                Quantity = 1,
                Attributes = new ItemAttributeDictionary(new Dictionary<AttributeKey, IBasketItemAttribute>
                {
                    {AttributeKey.Fax, new FaxAttributes("+32.3", "3466262", false, "fax@email.com", null) }
                })
            };

            yield return new OrderItemContract
            {
                ParentId = 1,
                Period = 12,
                PeriodUnitType = PeriodUnitTypeContract.OneYear,
                PromoCode = "setup-promo",
                Price = new PriceContract
                {
                    ExclVat = 50,
                    InclVat = 56
                },
                ProductCode = "eenmalige-activatie-faxnummer",
                ProductId = 7095,
                PeriodId = 454,
                ProductName = "Eénmalige activatie faxnummer",
                Attributes = new ItemAttributeDictionary()
            };
        }

        [Test]
        public void IsMappedToCorrectFaxPackage()
        {
            MappedOrder.FaxPackages.Should().HaveCount(1);
            var faxPackage = MappedOrder.FaxPackages.First();
            faxPackage.ID.Should().Be(6138);
            faxPackage.PeriodID.Should().Be(1234);
            faxPackage.Period.Should().Be(12);
            faxPackage.Price.Should().Be(100);
            faxPackage.StandardPrice.Should().Be(100);
            faxPackage.DiscountPercentage.Should().Be(0);
            faxPackage.Name.Should().Be("Fax Services");
            faxPackage.FaxNumber.Should().Be("+32.33466262");
            faxPackage.FaxAction.Should().Be(ORF_FaxActions.TransferFaxNumber);
            faxPackage.Email.Should().Be("fax@email.com");
            faxPackage.FixedCosts.Should().HaveCount(1);

            var fixedCost = faxPackage.FixedCosts.First();
            fixedCost.Discount.Should().Be(0);
            fixedCost.ID.Should().Be(7095);
            fixedCost.Name.Should().Be("Eénmalige activatie faxnummer");
            fixedCost.PeriodID.Should().Be(454);
            fixedCost.Price.Should().Be(50);
            fixedCost.PromotionCode = "setup-promo";
            fixedCost.StandardPrice.Should().Be(50);

            MappedOrder.ManualActivations.Should().HaveCount(0);
        }
    }
}