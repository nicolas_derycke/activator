﻿using Combell.Model.OrderForm;
using FluentAssertions;
using NUnit.Framework;
using Store.API.Contracts;
using Store.API.Contracts.Orders;
using Store.API.Contracts.TypedItemAttributes;
using System.Collections.Generic;
using System.Linq;
using Activator.Services;
using Combell.Model.UAC;
using Combell.Model.UAC.ResourceSets;
using Rhino.Mocks;

namespace Activator.Service.Tests.OrderContractToOrfOrderMapping
{
    public class WhenMappingALinuxHostingUpgrade : MappingTest
    {
        protected override void StubServicePackage(IServicePackageProvider servicePackageProvider)
        {
            servicePackageProvider
           .Stub(x => x.GetServicePackage(154, 0))
           .Return(new UAC_ServicePackage
           {
               ResourceSets = new List<UAC_ResourceSet>
               {
                        new UAC_HostingResourceSet
                        {
                            ResourceSets = new List<UAC_ResourceSet>
                            {
                                new UAC_LinuxHostingResourceSet()
                            }
                        }
               },
           });
        }

        protected override IEnumerable<OrderItemContract> GetOrderItemsToMap()
        {
            yield return new OrderItemContract()
                        {
                            Attributes = new ItemAttributeDictionary()
                            {
                                {AttributeKey.Domain, new DomainItemAttributes("zeno.be")},
                                {AttributeKey.Hosting, new HostingAttributes(956)}
                            },
                            ProductId = 154,
                            PeriodId = 5,
                            Price = new PriceContract()
                            {
                                ExclVat = 75
                            },
                            Period = 12,
                            ProductCode = "linux-business",
                            ProductType = "linux-shared-hosting",
                            ProductName = "Linux business",
                            Quantity = 1,
                            OrderItemId = 111
                        };

            yield return new OrderItemContract()
            {
                Attributes = new ItemAttributeDictionary()
                            {
                                {AttributeKey.Domain, new DomainItemAttributes("zeno.be")},
                                {AttributeKey.Compensation, new CompensationAttributes(98756) }
                            },
                ProductId = 999,
                PeriodId = 5,
                Price = new PriceContract()
                {
                    ExclVat = 65
                },
                Period = 12,
                ProductCode = "linux-express",
                ProductType = "linux-shared-hosting",
                ProductName = "Linux express",
                Quantity = 1,
                OrderItemId = 222,
                ParentId = 111
            };
        }

        [Test]
        public void ShouldOrfOrderContainLinuxUpgradeData()
        {
            MappedOrder.Should().NotBeNull();
            MappedOrder.UpgradeHostingPackages.Should().NotBeNull();
            MappedOrder.UpgradeHostingPackages.Should().HaveCount(1);

            var upgradeHostingPackage = MappedOrder.UpgradeHostingPackages.First();
            upgradeHostingPackage.ID.Should().Be(154);
            upgradeHostingPackage.PeriodID.Should().Be(5);
            upgradeHostingPackage.Period.Should().Be(12);
            upgradeHostingPackage.Price.Should().Be(75);
            upgradeHostingPackage.StandardPrice.Should().Be(75);
            upgradeHostingPackage.DiscountPercentage.Should().Be(0);
            upgradeHostingPackage.Name.Should().Be("Linux business");
            upgradeHostingPackage.Identifier.Should().Be("zeno.be");
            upgradeHostingPackage.AccountId.Should().Be(956);
        }
    }
}