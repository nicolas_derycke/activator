﻿using Combell.Model.OrderForm;
using FluentAssertions;
using NUnit.Framework;
using Store.API.Contracts;
using Store.API.Contracts.Orders;
using Store.API.Contracts.TypedItemAttributes;
using System.Collections.Generic;
using System.Linq;
using Activator.Services;
using Combell.Model.UAC;
using Combell.Model.UAC.ResourceSets;
using Rhino.Mocks;

namespace Activator.Service.Tests.OrderContractToOrfOrderMapping
{
    public class WhenMappingALinuxHostingWithDomainRegistration : MappingTest
    {
        protected override void StubServicePackage(IServicePackageProvider servicePackageProvider)
        {
            servicePackageProvider
                .Stub(x => x.GetServicePackage(154, 0))
                .Return(new UAC_ServicePackage
                {
                    ResourceSets = new List<UAC_ResourceSet>
                    {
                        new UAC_HostingResourceSet
                        {
                            ResourceSets = new List<UAC_ResourceSet>
                            {
                                new UAC_LinuxHostingResourceSet()
                            }
                        }
                    },
                });

            servicePackageProvider
                .Stub(x => x.GetServicePackage(155, 0))
                .Return(new UAC_ServicePackage
                {
                    ResourceSets = new List<UAC_ResourceSet>
                    {
                        new UAC_HostingResourceSet
                        {
                            ResourceSets = new List<UAC_ResourceSet>
                            {
                                new UAC_DomainnamesResourceSet()
                            }
                        }
                    },
                });
        }

        protected override IEnumerable<OrderItemContract> GetOrderItemsToMap()
        {
            yield return new OrderItemContract()
                        {
                            Attributes = new ItemAttributeDictionary()
                            {
                                {AttributeKey.Domain, new DomainItemAttributes("zeno.be")}
                            },
                            ProductId = 154,
                            PeriodId = 5,
                            Price = new PriceContract()
                            {
                                ExclVat = 75
                            },
                            Period = 12,
                            ProductCode = "linux-express",
                            ProductType = "linux-shared-hosting",
                            ProductName = "Linux express",
                            Quantity = 1
                        };

            yield return new OrderItemContract()
            {
                Attributes = new ItemAttributeDictionary()
                            {
                                {AttributeKey.Domain, new DomainItemAttributes("zeno.be")},
                                {
                                    AttributeKey.Registrant, 
                                    new RegistrantAttributes(
                                        "Gent",
                                        string.Empty, 
                                        "be", 
                                        "zeno@combellgroup.com", 
                                        string.Empty, 
                                        "zeno", 
                                        string.Empty, 
                                        "nl", 
                                        "Dierick", 
                                        "9000", 
                                        "Jan Delvinlaan", 
                                        "+32.479893903", 
                                        "102", 
                                        string.Empty)
                                }
                            },
                ProductId = 155,
                PeriodId = 9,
                Price = new PriceContract()
                {
                    ExclVat = 25,
                    ReductionExclVat = 25
                },
                Period = 24,
                ProductCode = "domeinnaam-be-promo",
                ProductType = "domainname",
                ProductName = "Domeinnaam .be",
                Quantity = 1
            };
        }

        [Test]
        public void ShouldOrfOrderContainLinuxData()
        {
            MappedOrder.Should().NotBeNull();
            MappedOrder.HostingPackages.Should().NotBeNull();
            MappedOrder.HostingPackages.Should().HaveCount(1);

            var hostingPackage = MappedOrder.HostingPackages.First();
            hostingPackage.ID.Should().Be(154);
            hostingPackage.PeriodID.Should().Be(5);
            hostingPackage.Period.Should().Be(12);
            hostingPackage.Price.Should().Be(75);
            hostingPackage.StandardPrice.Should().Be(75);
            hostingPackage.DiscountPercentage.Should().Be(0);
            hostingPackage.Name.Should().Be("Linux express");

            hostingPackage.Domains.Should().NotBeNull();
            hostingPackage.Domains.Should().HaveCount(1);

            var domain = hostingPackage.Domains.First();
            domain.ID.Should().Be(155);
            domain.PeriodID.Should().Be(9);
            domain.Period.Should().Be(24);
            domain.Price.Should().Be(25);
            domain.StandardPrice.Should().Be(50);
            domain.DiscountPercentage.Should().Be(0.5m);
            domain.DomainName.Should().Be("zeno.be");
            domain.NameServerAction.Should().Be(NameServerActionTypes.Forwarding);
            domain.IsMainDomain.Should().BeTrue();
            domain.DomainAction.Should().Be(DomainActionTypes.Register);

            var registrant = domain.RegistrantContact;
            registrant.Should().NotBeNull();
            registrant.City.Should().Be("Gent");
            registrant.Country.Should().Be("BE");
            registrant.Email.Should().Be("zeno@combellgroup.com");
            registrant.FirstName.Should().Be("zeno");
            registrant.Language.Should().Be("nl");
            registrant.Name.Should().Be("Dierick");
            registrant.PostalCode.Should().Be("9000");
            registrant.Street.Should().Be("Jan Delvinlaan");
            registrant.Telephone.Should().Be("+32.479893903");
            registrant.HouseNumber.Should().Be("102");
        }
    }
}
