﻿using Combell.Model.OrderForm;
using FluentAssertions;
using NUnit.Framework;
using Store.API.Contracts;
using Store.API.Contracts.Orders;
using Store.API.Contracts.TypedItemAttributes;
using System.Collections.Generic;
using System.Linq;
using Activator.Services;
using Combell.Model.UAC;
using Combell.Model.UAC.Enumerations;
using Combell.Model.UAC.ResourceSets;
using Rhino.Mocks;

namespace Activator.Service.Tests.OrderContractToOrfOrderMapping
{
    public class WhenMappingADomainNameTransfer : MappingTest
    {
        protected override void StubServicePackage(IServicePackageProvider servicePackageProvider)
        {
            servicePackageProvider
           .Stub(x => x.GetServicePackage(154, 0))
           .Return(new UAC_ServicePackage
           {
               ResourceSets = new List<UAC_ResourceSet>
               {
                        new UAC_HostingResourceSet
                        {
                            ResourceSets = new List<UAC_ResourceSet>
                            {
                                new UAC_DomainnamesResourceSet()
                            }
                        }
               }
           });
        }

        protected override IEnumerable<OrderItemContract> GetOrderItemsToMap()
        {
            yield return new OrderItemContract()
                        {
                            Attributes = new ItemAttributeDictionary()
                            {
                                {AttributeKey.Domain, new DomainItemAttributes("zeno.be")},
                                {AttributeKey.TransferDomain, new TransferDomainAttributes(string.Empty, false, false)},
                                {
                                    AttributeKey.Nameservers, 
                                    new NameserversAttributes()
                                    {
                                        new NameserverAttributes("ns3.combell.net", "12.3.5.6"),
                                        new NameserverAttributes("ns4.combell.net", "12.3.5.7")
                                    }
                                },
                                {
                                    AttributeKey.Registrant, 
                                    new RegistrantAttributes(
                                        "Gent",
                                        string.Empty, 
                                        "be", 
                                        "zeno@combellgroup.com", 
                                        string.Empty, 
                                        "zeno", 
                                        string.Empty, 
                                        "nl", 
                                        "Dierick", 
                                        "9000", 
                                        "Jan Delvinlaan", 
                                        "+32.479893903", 
                                        "102", 
                                        string.Empty)
                                }
                            },
                            ProductId = 154,
                            PeriodId = 5,
                            Price = new PriceContract()
                            {
                                ExclVat = 75,
                                ReductionExclVat = 25
                            },
                            Period = 12,
                            ProductCode = "domeinnaam-be-promo",
                            ProductType = "domainname",
                            ProductName = "Domeinnaam .be",
                            Quantity = 1
                        };
        }

        [Test]
        public void ShouldOrfOrderContainDomainNameData()
        {
            MappedOrder.Should().NotBeNull();
            MappedOrder.SingleDomains.Should().NotBeNull();
            MappedOrder.SingleDomains.Should().HaveCount(1);

            var singleDomain = MappedOrder.SingleDomains.First();
            singleDomain.ID.Should().Be(154);
            singleDomain.PeriodID.Should().Be(5);
            singleDomain.Period.Should().Be(12);
            singleDomain.Price.Should().Be(75);
            singleDomain.StandardPrice.Should().Be(100);
            singleDomain.DiscountPercentage.Should().Be(0.25m);
            singleDomain.DomainName.Should().Be("zeno.be");
            singleDomain.NameServerAction.Should().Be(NameServerActionTypes.OwnNameservers);
            singleDomain.IsMainDomain.Should().BeFalse();
            singleDomain.DomainAction.Should().Be(DomainActionTypes.Transfer);
            singleDomain.Name.Should().Be("Domeinnaam .be");
            singleDomain.ExtraFields.Should().BeEmpty();

            singleDomain.NameServers.Should().NotBeNull();
            singleDomain.NameServers.Should().HaveCount(2);

            var firstNs = singleDomain.NameServers[0];
            var secondNs = singleDomain.NameServers[1];

            firstNs.Name.Should().Be("ns3.combell.net");
            firstNs.Ip.Should().Be("12.3.5.6");

            secondNs.Name.Should().Be("ns4.combell.net");
            secondNs.Ip.Should().Be("12.3.5.7");

            var registrant = singleDomain.RegistrantContact;
            registrant.Should().NotBeNull();
            registrant.City.Should().Be("Gent");
            registrant.Country.Should().Be("BE");
            registrant.Email.Should().Be("zeno@combellgroup.com");
            registrant.FirstName.Should().Be("zeno");
            registrant.Language.Should().Be("nl");
            registrant.Name.Should().Be("Dierick");
            registrant.PostalCode.Should().Be("9000");
            registrant.Street.Should().Be("Jan Delvinlaan");
            registrant.Telephone.Should().Be("+32.479893903");
            registrant.HouseNumber.Should().Be("102");
        }
    }
}
