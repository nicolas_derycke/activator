﻿using Combell.Model.OrderForm;
using FluentAssertions;
using NUnit.Framework;
using Store.API.Contracts;
using Store.API.Contracts.Orders;
using Store.API.Contracts.TypedItemAttributes;
using System.Collections.Generic;
using System.Linq;
using Activator.Services;
using Combell.Model.UAC;
using Combell.Model.UAC.ResourceSets;
using Rhino.Mocks;

namespace Activator.Service.Tests.OrderContractToOrfOrderMapping
{
    public class WhenMappingABasicEmail : MappingTest
    {
        protected override void StubServicePackage(IServicePackageProvider servicePackageProvider)
        {
            servicePackageProvider
           .Stub(x => x.GetServicePackage(154, 0))
           .Return(new UAC_ServicePackage
           {
               ResourceSets = new List<UAC_ResourceSet>
               {
                        new UAC_HostingResourceSet
                        {
                            ResourceSets = new List<UAC_ResourceSet>
                            {
                                new UAC_BasicEmailResourceSet()
                            }
                        }
               },
           });
        }

        protected override IEnumerable<OrderItemContract> GetOrderItemsToMap()
        {
            yield return new OrderItemContract()
                        {
                            Attributes = new ItemAttributeDictionary()
                            {
                                {AttributeKey.Domain, new DomainItemAttributes("zeno.be")}
                            },
                            ProductId = 154,
                            PeriodId = 5,
                            Price = new PriceContract()
                            {
                                ExclVat = 25,
                                ReductionExclVat = 25
                            },
                            Period = 12,
                            ProductCode = "basic-e-mail-5-gb",
                            ProductType = "basic-e-mail",
                            ProductName = "Office Entry 5 GB",
                            Quantity = 1
                        };
        }

        [Test]
        public void ShouldOrfOrderContainBasicEmailData()
        {
            MappedOrder.Should().NotBeNull();
            MappedOrder.HostedExchangeDomainPackages.Should().NotBeNull();
            MappedOrder.HostedExchangeDomainPackages.Should().HaveCount(1);

            var hostedExchangeDomainPackage = MappedOrder.HostedExchangeDomainPackages.First();

            hostedExchangeDomainPackage.HostedExchangePackages.Should().NotBeNull();
            hostedExchangeDomainPackage.HostedExchangePackages.Should().HaveCount(1);

            var hostedExchangePackages = hostedExchangeDomainPackage.HostedExchangePackages.First();
            hostedExchangePackages.ID.Should().Be(154);
            hostedExchangePackages.PeriodID.Should().Be(5);
            hostedExchangePackages.Period.Should().Be(12);
            hostedExchangePackages.Price.Should().Be(25);
            hostedExchangePackages.StandardPrice.Should().Be(50);
            hostedExchangePackages.DiscountPercentage.Should().Be(0.5m);
            hostedExchangePackages.Name.Should().Be("Office Entry 5 GB");

            hostedExchangeDomainPackage.Domains.Should().NotBeNull();
            hostedExchangeDomainPackage.Domains.Should().HaveCount(1);

            var domain = hostedExchangeDomainPackage.Domains.First();
            domain.DomainName.Should().Be("zeno.be");
            domain.NameServerAction.Should().Be(NameServerActionTypes.Forwarding);
            domain.IsMainDomain.Should().BeTrue();
            domain.DomainAction.Should().Be(DomainActionTypes.NoAction);
        }
    }
}
