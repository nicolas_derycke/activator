﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Activator.Commands;
using Activator.Proxies;
using Activator.Queries;
using Activator.Queries.Data;
using Activator.Services.Activators;
using Core.Templating.API.Client;
using Core.Templating.API.Contracts;
using FluentAssertions;
using Intelligent.Core.Messages.Commands;
using Intelligent.Core.Models;
using Intelligent.Shared.Core.Commands;
using Intelligent.Shared.Core.Queries;
using Intelligent.Shared.Testing.NUnit;
using MassTransit;
using NUnit.Framework;
using Rhino.Mocks;
using Store.API.Contracts;
using Store.API.Contracts.Orders;
using Store.API.Contracts.TypedItemAttributes;

namespace Activator.Service.Tests.PreRegistrationActivation
{
    [TestFixture]
    public class WhenActivatingPreRegistration : GivenWhenThen
    {
        private IQueryHandler _queryHandler;
        private ICommandHandler _commandHandler;
        private IBus _bus;
        private ITemplateClient _templateClient;
        private ICustomerApiProxy _customerApiProxy;

        private PreRegistrationActivator _preRegistrationActivator;

        private CustomerIdentifier _customerIdentifier;
        private string _orderCode;
        private IEnumerable<OrderItemContract> _preRegistrations;
        private string _firstDomain;
        private string _secondDomain;
        private Template _template;
        private Template _singleDomainRowTemplate;
        private Template _otherCustomerPreRegisteredDomainWarningTemplate;

        private string _body;

        protected override void Given()
        {
            _queryHandler = MockRepository.GenerateMock<IQueryHandler>();
            _commandHandler = MockRepository.GenerateMock<ICommandHandler>();
            _bus = MockRepository.GenerateMock<IBus>();
            _templateClient = MockRepository.GenerateMock<ITemplateClient>();
            _customerApiProxy = MockRepository.GenerateMock<ICustomerApiProxy>();

            _preRegistrationActivator = new PreRegistrationActivator(_queryHandler, _commandHandler, _bus, _templateClient, _customerApiProxy);

            _customerIdentifier = CustomerIdentifier.FromCustomerNumber(129829);
            _orderCode = "COM-fsd15fsd5s";
            _firstDomain = "zeno.be";
            _secondDomain = "dierick.be";
            _preRegistrations = new List<OrderItemContract>()
                {
                    new OrderItemContract()
                    {
                        Attributes = new ItemAttributeDictionary()
                        {
                            { AttributeKey.Domain, new DomainItemAttributes(_firstDomain) }
                        }
                    },
                    new OrderItemContract()
                    {
                        Attributes = new ItemAttributeDictionary()
                        {
                            { AttributeKey.Domain, new DomainItemAttributes(_secondDomain) }
                        }
                    }
                };

            var customer = CustomerBuilder.GetCustomer("nl");
            var provider = new Provider((ProviderValue)Enum.Parse(typeof(ProviderValue), customer.Provider, true));
            var language = Language.Dutch;

            _customerApiProxy.
                Expect(x => x.Get(_customerIdentifier)).
                Return(Task.FromResult(customer)).
                Repeat.Once();

            _template = new Template() { From = "from", Subject = "cool subject", Body = "the body $$GTLD_ORDERS$$ $$EXISTINGDOMAINS$$" };
            _templateClient.
                Expect(x => x.GetTemplate("@@order_by_existing_customer", provider, language)).
                Return(Task.FromResult(_template)).
                Repeat.Once();

            _singleDomainRowTemplate = new Template() { Body = "single row $$DOMAIN$$" };
            _templateClient.
                Expect(x => x.GetTemplate("@@gtld_domain", provider, language)).
                Return(Task.FromResult(_singleDomainRowTemplate)).
                Repeat.Once();

            _otherCustomerPreRegisteredDomainWarningTemplate = new Template() { Body = "duplicate warning" };
            _templateClient.
                Expect(x => x.GetTemplate("@@gtld_existingdomains", provider, language)).
                Return(Task.FromResult(_otherCustomerPreRegisteredDomainWarningTemplate)).
                Repeat.Once();

            _queryHandler.
                Expect(x => x.Execute(Arg<GetGtldDomainsByNameQuery>.Matches(arg => arg.DomainName.FullName == _firstDomain))).
                Return(new[] { new GtldDomain(1, _customerIdentifier.CustomerId + 1, _firstDomain, new DateTime(1900, 1, 1), new DateTime(1900, 1, 1)) }).
                Repeat.Once();

            _commandHandler.
                Expect(x => x.Execute(Arg<CreateGtldDomainCommand>.Matches(arg => arg.DomainName.FullName == _firstDomain && arg.CustomerIdentifier == _customerIdentifier))).
                Repeat.Once();

            _queryHandler.
                Expect(x => x.Execute(Arg<GetGtldDomainsByNameQuery>.Matches(arg => arg.DomainName.FullName == _secondDomain))).
                Return(new[] { new GtldDomain(1, _customerIdentifier.CustomerId, _secondDomain, new DateTime(1900, 1, 1), new DateTime(1900, 1, 1)) }).
                Repeat.Once();

            _commandHandler.
                Expect(x => x.Execute(Arg<CreateGtldDomainCommand>.Matches(arg => arg.DomainName.FullName == _secondDomain && arg.CustomerIdentifier == _customerIdentifier))).
                Repeat.Never();

            _bus.
                Expect(x => x.Publish(Arg<SendEmailCommand>.Matches(arg =>
                    !string.IsNullOrWhiteSpace(arg.Body) &&
                    arg.From == _template.From &&
                    arg.Subject == _template.Subject &&
                    arg.Ordercode == _orderCode &&
                    arg.CustomerId == _customerIdentifier.CustomerId &&
                    arg.CustomerNumber == _customerIdentifier.CustomerNumber),
                    Arg<CancellationToken>.Is.Anything)).
                WhenCalled(a =>
                {
                    _body = ((SendEmailCommand)a.Arguments[0]).Body;
                }).
                Return(Task.FromResult(0)).
                Repeat.Once();
        }

        protected override void When()
        {
            _preRegistrationActivator.Activate(_customerIdentifier, _orderCode, _preRegistrations).Wait();
        }

        [Test]
        public void ShouldSendEmail()
        {
            _bus.VerifyAllExpectations();
            _body.Should().Contain(_firstDomain);
            _body.Should().Contain(_secondDomain);
            _body.Should().Contain(_otherCustomerPreRegisteredDomainWarningTemplate.Body);
        }

        [Test]
        public void ShouldCreateGtldDomains()
        {
            _queryHandler.VerifyAllExpectations();
            _commandHandler.VerifyAllExpectations();
        }

        [Test]
        public void ShouldNotCreateExistingGtldDomains()
        {
            _queryHandler.VerifyAllExpectations();
            _commandHandler.VerifyAllExpectations();
        }
    }
}