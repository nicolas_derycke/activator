﻿using Activator.Messages;
using Activator.Messages.Commands;
using Activator.Service.CommandHandlers;
using Intelligent.Shared.Testing.NUnit;
using MassTransit;
using NUnit.Framework;
using Rhino.Mocks;
using Serilog;
using System.Threading.Tasks;
using Activator.Services.Activators;

namespace Activator.Service.Tests.ActivateOrderHandling
{
    [TestFixture]
    public class WhenAnOrderActivationForStoreOrderIsRequested : GivenWhenThen
    {
        private ActivateOrderHandler _activateOrderHandler;
        private IOrderActivator _orderActivator;

        private string _orderCode;
        private ConsumeContext<ActivateOrder> _context;

        protected override void Given()
        {
            _orderActivator = MockRepository.GenerateMock<IOrderActivator>();
            var logger = MockRepository.GenerateMock<ILogger>();
            _activateOrderHandler = new ActivateOrderHandler(_orderActivator, logger);

            _context = MockRepository.GenerateMock<ConsumeContext<ActivateOrder>>();
            _orderCode = "COM-20160321-100";
            var activationReason = "I activated this thingy";
            var activationMethod = ActivationMethod.Manual;
            var activationById = 135006;
            var activationBy = "Guillaume";

            _context
                .Expect(x => x.Message)
                .Return(new ActivateOrder(_orderCode, activationMethod, activationReason, activationById, activationBy))
                .Repeat.AtLeastOnce();

            _orderActivator
                .Expect(x => x.ActivateOrder(_orderCode, activationMethod, activationReason, activationById, activationBy))
                .Return(Task.FromResult(0))
                .Repeat.Once();
        }

        protected override void When()
        {
            _activateOrderHandler.Consume(_context).Wait();
        }

        [Test]
        public void ShouldActivateOrder()
        {
            _orderActivator.VerifyAllExpectations();
        }
    }
}
