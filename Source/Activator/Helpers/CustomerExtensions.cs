﻿using System;
using System.Linq;
using Customer.API.Contracts;

namespace Activator.Helpers
{
    public static class CustomerExtensions
    {
        public static AddressContract GetAddress(this CustomerContract customer, AddressTypeContract addressType)
        {
            var invoicingAddress = customer.Addresses.FirstOrDefault(a => a.AddressType == addressType);
            if (invoicingAddress == null)
                throw new Exception(string.Format("Customer {0} has no invoicing address", customer.CustomerNumber));
            return invoicingAddress;
        }

        public static ContactData GetContactData(this CustomerContract customer, ContactTypeContract contactType)
        {
            var contact = customer.Contacts.FirstOrDefault(c => c.ContactType == contactType);
            if (contact == null)
                throw new Exception(string.Format("Customer {0} has no primary contact", customer.CustomerNumber));
            return new ContactData(contact);
        }

        public static ContactData GetContactDataOrEmpty(this CustomerContract customer, ContactTypeContract contactType)
        {
            var contact = customer.Contacts.FirstOrDefault(c => c.ContactType == contactType);
            if (contact == null)
                return ContactData.Empty();
            return new ContactData(contact);
        }

        public static string GetEmail(this ContactContract contact)
        {
            var contactEmail = contact.EmailAddress;
            if (string.IsNullOrEmpty(contactEmail))
                throw new Exception(string.Format("Contact {0} does not have an email", contact.Id));
            return contactEmail;
        }

        public static string GetPhoneNumber(this ContactContract contact, PhoneTypeContract phoneType)
        {
            var phone = contact.Phones.FirstOrDefault(p => p.Type == phoneType);

            if (phone == null)
                return string.Empty;

            return phone.Number;
        }
    }
}
