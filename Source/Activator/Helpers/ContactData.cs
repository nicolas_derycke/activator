﻿using System;
using Customer.API.Contracts;
using Intelligent.Core.Mappers;
using Intelligent.Core.Models;

namespace Activator.Helpers
{
    public class ContactData
    {
        public string FirstName { get; private set; }

        public string LastName { get; private set; }

        public string Fax { get; private set; }

        public string Mobile { get; private set; }

        public string Telephone1 { get; private set; }

        public string Email { get; private set; }

        public Language Language { get; private set; }

        public int LanguageId
        {
            get
            {
                return (int)Language;
            }
        }

        public ContactData(string firstName, string lastName, string fax, string mobile, string telephone1, string email, string language)
        {
            FirstName = firstName;
            LastName = lastName;
            Fax = fax;
            Mobile = mobile;
            Telephone1 = telephone1;
            Email = email;
            Language outLanguage;
            if (!LanguageMapper.TryMapLanguage(language, out outLanguage))
                throw new ArgumentException("language not supported", "language");
            Language = outLanguage;
        }

        public ContactData(ContactContract contact)
            : this(
                contact.FirstName,
                contact.LastName,
                contact.GetPhoneNumber(PhoneTypeContract.Fax),
                contact.GetPhoneNumber(PhoneTypeContract.Mobile),
                contact.GetPhoneNumber(PhoneTypeContract.Regular),
                contact.GetEmail(),
                "nl")
        {
            Language mappedLanguage;

            if (!LanguageMapper.TryMapLanguage(contact.Language, out mappedLanguage))
                throw new Exception(string.Format("Invalid language '{0}'", contact.Language));
            Language = mappedLanguage;
        }

        public static ContactData Empty()
        {
            return new ContactData(string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, "nl");
        }
    }
}