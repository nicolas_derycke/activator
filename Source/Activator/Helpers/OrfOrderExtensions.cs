﻿using System.Collections.Generic;
using System.Linq;
using Combell.Model.OrderForm;
using Store.API.Contracts.Orders;

namespace Activator.Helpers
{
    public static class OrfOrderExtensions
    {
        public static void SwapByDomainName(this List<ORF_Domain> domains, ORF_Domain orfDomain)
        {
            domains.RemoveAll(d => d.DomainName == orfDomain.DomainName);
            domains.Add(orfDomain);
        }

        public static bool ContainsDomainName(this List<ORF_Domain> domains, string domainName)
        {
            return domains.Any(d => d.DomainName == domainName);
        }

        public static void SetPackagePriceInformation(this IHavePackagePriceInformation packagePriceInformation, OrderItemContract orderItem)
        {
            packagePriceInformation.ID = orderItem.ProductId;
            packagePriceInformation.PeriodID = orderItem.PeriodId;
            packagePriceInformation.Period = orderItem.Period;
            packagePriceInformation.Price = orderItem.Price.ExclVat;
            packagePriceInformation.StandardPrice = orderItem.Price.ExclVat + orderItem.Price.ReductionExclVat;
            if (packagePriceInformation.Price != 0 && packagePriceInformation.StandardPrice != 0)
                packagePriceInformation.DiscountPercentage = 1 - (packagePriceInformation.Price / packagePriceInformation.StandardPrice);
            else
                packagePriceInformation.DiscountPercentage = 0;
            packagePriceInformation.Name = orderItem.ProductName;
        }

        public static ORF_Domain CreateNoActionOrfDomain(string domainName)
        {
            var orfDomain = new ORF_Domain();
            orfDomain.DomainName = domainName;
            orfDomain.NameServerAction = NameServerActionTypes.Forwarding;
            orfDomain.DomainAction = DomainActionTypes.NoAction;
            orfDomain.IsMainDomain = true;
            return orfDomain;
        }
    }
}