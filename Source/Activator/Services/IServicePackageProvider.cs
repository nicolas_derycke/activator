using Combell.Model.UAC;
using Intelligent.Shared.Core;

namespace Activator.Services
{
    public interface IServicePackageProvider
    {
        Optional<UAC_ServicePackage> GetServicePackage(int productId, int providerId);
    }
}