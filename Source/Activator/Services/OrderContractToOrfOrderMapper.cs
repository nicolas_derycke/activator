﻿using Activator.Helpers;
using Activator.Proxies;
using Activator.Services.Mapping;
using Combell.Model.OrderForm;
using Combell.Model.OrderQueue;
using Customer.API.Contracts;
using Intelligent.Core.Models;
using Serilog;
using Store.API.Contracts.Orders;
using Store.API.Contracts.Specifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Activator.Services
{
    public class OrderContractToOrfOrderMapper : IOrderContractToOrfOrderMapper
    {
        private readonly ICustomerApiProxy _customerApiProxy;
        private readonly ICustomerServiceProxy _customerServiceProxy;
        private readonly IEnumerable<IOrderItemToProvisioningOrderMapper> _itemMappers;
        private readonly IServicePackageProvider _servicePackageProvider;
        private readonly IManualActivationMapper _manualActivationMapper;
        private readonly ILogger _logger;

        public OrderContractToOrfOrderMapper(
            ICustomerApiProxy customerApiProxy,
            ICustomerServiceProxy customerServiceProxy,
            IEnumerable<IOrderItemToProvisioningOrderMapper> itemMappers,
            IServicePackageProvider servicePackageProvider,
            IManualActivationMapper manualActivationMapper,
            ILogger logger)
        {
            if (customerApiProxy == null) throw new ArgumentNullException("customerApiProxy");
            if (customerServiceProxy == null) throw new ArgumentNullException("customerServiceProxy");
            if (itemMappers == null) throw new ArgumentNullException("itemMappers");
            if (servicePackageProvider == null) throw new ArgumentNullException("servicePackageProvider");
            if (manualActivationMapper == null) throw new ArgumentNullException("manualActivationMapper");
            if (logger == null) throw new ArgumentNullException("logger");

            _customerApiProxy = customerApiProxy;
            _customerServiceProxy = customerServiceProxy;
            _itemMappers = itemMappers;
            _servicePackageProvider = servicePackageProvider;
            _manualActivationMapper = manualActivationMapper;
            _logger = logger;
        }

        public async Task<ORF_Order> Map(CustomerIdentifier customerIdentifier, OrderContract order)
        {
            var orderItems = order.Items.Where(x => !x.IsPreRegistration());

            var customer = await _customerApiProxy.Get(customerIdentifier);
            var provider = new Provider((ProviderValue)Enum.Parse(typeof(ProviderValue), customer.Provider, true));

            var primaryContact = customer.GetContactData(ContactTypeContract.Primary);

            var technicalContact = customer.GetContactDataOrEmpty(ContactTypeContract.Technical);

            var invoicingAddress = customer.GetAddress(AddressTypeContract.Invoicing);

            var customerInvoiceCriteria = _customerServiceProxy.GetCustomerInvoiceCriteria(customerIdentifier);

            var orfOrder = new ORF_Order();
            orfOrder.OrderCode = order.OrderCode;
            orfOrder.OrderOrigin = ORF_OrderOrigins.Store;
            orfOrder.OrderPriority = OrderQueuePriority.High;
            orfOrder.Ticket = new ORF_Ticket() { TicketId = order.TicketId, TicketNumber = order.TicketNumber };
            orfOrder.OrderCustomer = new ORF_Customer()
            {
                Company = customer.CompanyName,
                Country = invoicingAddress.CountryCode,
                CountryVATCode = customer.VatNumber.CountryCode,
                DeliveryCompany = provider.Id,
                EMail = primaryContact.Email,
                Fax = primaryContact.Fax,
                FirstName = primaryContact.FirstName,
                Gsm = primaryContact.Mobile,
                HouseNumber = invoicingAddress.Number,
                ID = customerIdentifier.CustomerId,
                IsReseller = customerInvoiceCriteria.DirectActivation,
                LanguageId = primaryContact.LanguageId,
                Name = primaryContact.LastName,
                Place = invoicingAddress.City,
                PostalCode = invoicingAddress.PostalCode,
                Street = invoicingAddress.Street,
                TechContact = (technicalContact.FirstName + " " + technicalContact.LastName).Trim(),
                TechContactFirstName = technicalContact.FirstName,
                TechContactLastName = technicalContact.LastName,
                TechEmail = technicalContact.Email,
                TechGSM = technicalContact.Mobile,
                TechTel = technicalContact.Telephone1,
                Telephone1 = primaryContact.Telephone1,
                VATNumber = customer.VatNumber.Number
            };

            var parentItems = orderItems.Where(i => !i.ParentId.HasValue);

            foreach (var orderItem in parentItems)
            {
                var childs = GetChildItems(orderItems, orderItem.OrderItemId).ToList();
                var servicePackage = _servicePackageProvider.GetServicePackage(orderItem.ProductId, order.ProviderId);
                if (servicePackage.HasValue)
                    _logger.Debug("Used {@servicePackage} for order item {@orderItem}", servicePackage, orderItem);

                var isMapped = false;
                foreach (var itemMapper in _itemMappers.Where(x => x.CanMap(orderItem, servicePackage)))
                {
                    _logger.Debug("Used {mapperType} to map {@orderItem}", itemMapper.GetType().Name, orderItem);
                    itemMapper.MapItemToProvisioningOrder(orfOrder, orderItem, childs);
                    isMapped = true;
                }

                if (!isMapped)
                {
                    _logger.Debug("Used manual activation mapper for order {orderCode}", order.OrderCode);
                    _manualActivationMapper.MapItemToProvisioningOrder(orfOrder, orderItem, childs);
                }
                else
                {
                    _manualActivationMapper.MapDependingManualActivations(orfOrder, orderItem, childs, servicePackage);
                }

            }

            return orfOrder;
        }

        private static IEnumerable<OrderItemContract> GetChildItems(IEnumerable<OrderItemContract> allOrderItems, int parentId)
        {
            return allOrderItems.Where(i => i.ParentId.HasValue && i.ParentId.Value == parentId);
        }
    }
}