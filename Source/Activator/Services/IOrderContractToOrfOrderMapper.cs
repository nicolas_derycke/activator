using System.Threading.Tasks;
using Combell.Model.OrderForm;
using Intelligent.Core.Models;
using Store.API.Contracts.Orders;

namespace Activator.Services
{
    public interface IOrderContractToOrfOrderMapper
    {
        Task<ORF_Order> Map(CustomerIdentifier customerIdentifier, OrderContract orderContract);
    }
}