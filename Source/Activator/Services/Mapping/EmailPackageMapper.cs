﻿using Activator.Helpers;
using Combell.Model.OrderForm;
using Combell.Model.UAC;
using Combell.Model.UAC.Enumerations;
using Combell.Model.UAC.ResourceSets;
using Intelligent.Shared.Core;
using Store.API.Client;
using Store.API.Contracts;
using Store.API.Contracts.Orders;
using Store.API.Contracts.TypedItemAttributes;
using System.Collections.Generic;

namespace Activator.Services.Mapping
{
    public class EmailPackageMapper : IOrderItemToProvisioningOrderMapper
    {
        public void MapItemToProvisioningOrder(ORF_Order order, OrderItemContract orderItem, IEnumerable<OrderItemContract> childs)
        {
            var hostedExchangeDomainPackage = new ORF_HostedExchangeDomainPackage();

            var domainAttributes = orderItem.Attributes.GetBasketItemAttribute<DomainItemAttributes>(AttributeKey.Domain);

            var emailAttributes = orderItem.Attributes.GetOptionalBasketItemAttribute<EmailAttributes>(AttributeKey.Email);


            if (emailAttributes.HasValue &&
                emailAttributes.Value.UpgradeFromMailboxId.HasValue)
            {
                order.Remark = "ProductOptionEntity=" + emailAttributes.Value.UpgradeFromMailboxId.Value;
            }

                var hostedExchangePackage = new ORF_HostedExchangePackage();
                hostedExchangePackage.SetPackagePriceInformation(orderItem);
                hostedExchangePackage.Quantity = orderItem.Quantity;

                var orfHostingPackageDomain = OrfOrderExtensions.CreateNoActionOrfDomain(domainAttributes.DomainName);

                hostedExchangeDomainPackage.HostedExchangePackages.Add(hostedExchangePackage);
                hostedExchangeDomainPackage.Domains.Add(orfHostingPackageDomain);

                order.HostedExchangeDomainPackages.Add(hostedExchangeDomainPackage);
        }

        public bool CanMap(OrderItemContract orderItem, Optional<UAC_ServicePackage> servicePackage)
        {
            return servicePackage.GetActiveResourceSet<UAC_HostedExchangeResourceSet>(UAC_ServicePackageType.Basic).HasValue ||
                   servicePackage.GetActiveResourceSet<UAC_BasicEmailResourceSet>(UAC_ServicePackageType.Basic).HasValue;
        }
    }
}