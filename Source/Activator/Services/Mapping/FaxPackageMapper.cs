﻿using System.Collections.Generic;
using Activator.Helpers;
using Combell.Model.OrderForm;
using Combell.Model.UAC;
using Intelligent.Shared.Core;
using Store.API.Client;
using Store.API.Contracts;
using Store.API.Contracts.Orders;
using Store.API.Contracts.TypedItemAttributes;

namespace Activator.Services.Mapping
{
    public class FaxPackageMapper : IOrderItemToProvisioningOrderMapper
    {
        public bool CanMap(OrderItemContract orderItem, Optional<UAC_ServicePackage> servicePackage)
        {
            return orderItem.Attributes.ContainsKey(AttributeKey.Fax);
        }

        public void MapItemToProvisioningOrder(ORF_Order order, OrderItemContract orderItem, IEnumerable<OrderItemContract> childs)
        {
            var faxAttributes = orderItem.Attributes.GetBasketItemAttribute<FaxAttributes>(AttributeKey.Fax);

            var faxPackage = new ORF_FaxPackage
            {
                Email = faxAttributes.Email,
                FaxAction = faxAttributes.IsNewFaxNumber ? ORF_FaxActions.NewFaxNumber : ORF_FaxActions.TransferFaxNumber,
                FaxNumber = faxAttributes.Identifier
            };

            foreach (var child in childs)
            {
                var fixedCost = new ORF_FixedCost
                {
                    ID = child.ProductId,
                    PeriodID = child.PeriodId,
                    Price = child.Price.ExclVat,
                    StandardPrice = child.Price.ExclVat + child.Price.ReductionExclVat,
                    PromotionCode = child.PromoCode
                };

                if (fixedCost.Price != 0 && fixedCost.StandardPrice != 0)
                    fixedCost.Discount = 1 - (fixedCost.Price / fixedCost.StandardPrice);
                else
                    fixedCost.Discount = 0;
                fixedCost.Name = child.ProductName;

                faxPackage.FixedCosts.Add(fixedCost);
            }

            faxPackage.SetPackagePriceInformation(orderItem);

            order.FaxPackages.Add(faxPackage);
        }
    }
}