﻿using Activator.Helpers;
using Combell.Model.OrderForm;
using Combell.Model.UAC;
using Combell.Model.UAC.Enumerations;
using Combell.Model.UAC.ResourceSets;
using Intelligent.Shared.Core;
using Store.API.Client;
using Store.API.Contracts;
using Store.API.Contracts.Orders;
using Store.API.Contracts.TypedItemAttributes;
using System.Collections.Generic;
using System.Linq;

namespace Activator.Services.Mapping
{
    public class CertificateMapper : IOrderItemToProvisioningOrderMapper
    {
        public void MapItemToProvisioningOrder(ORF_Order order, OrderItemContract orderItem, IEnumerable<OrderItemContract> childs)
        {
            var certificateAttributes = orderItem.Attributes.GetBasketItemAttribute<CertificateAttributes>(AttributeKey.Certificate);
            var sslCertificate = new ORF_SSLCertificate();
            sslCertificate.SetPackagePriceInformation(orderItem);

            sslCertificate.MainHostname = certificateAttributes.Identifier;

            sslCertificate.MultiDomainHostnames = new List<ORF_SSLMultiDomainHostname>();

            foreach (var identifier in certificateAttributes.Identifiers.Where(i => i != certificateAttributes.Identifier))
            {
                var sslMultiDomainHostName = new ORF_SSLMultiDomainHostname();
                sslMultiDomainHostName.SetPackagePriceInformation(orderItem);
                sslMultiDomainHostName.Price = 0;
                sslMultiDomainHostName.Hostname = identifier;
                sslCertificate.MultiDomainHostnames.Add(sslMultiDomainHostName);
            }

            foreach (var child in childs)
            {
                var sslMultiDomainHostName = new ORF_SSLMultiDomainHostname();
                sslMultiDomainHostName.SetPackagePriceInformation(child);
                sslMultiDomainHostName.Hostname = child.Attributes.Identifier;
                sslCertificate.MultiDomainHostnames.Add(sslMultiDomainHostName);
            }

            order.SSLCertificates.Add(sslCertificate);
        }

        public bool CanMap(OrderItemContract orderItem, Optional<UAC_ServicePackage> servicePackage)
        {
            return servicePackage.GetActiveResourceSet<UAC_SSLResourceSet>(UAC_ServicePackageType.Basic).HasValue;
        }
    }
}