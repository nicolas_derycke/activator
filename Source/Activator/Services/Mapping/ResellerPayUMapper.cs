﻿using Activator.Helpers;
using Combell.Model.OrderForm;
using Combell.Model.UAC;
using Intelligent.Shared.Core;
using Store.API.Contracts.Orders;
using System.Collections.Generic;

namespace Activator.Services.Mapping
{
    public class ResellerPayUMapper : IOrderItemToProvisioningOrderMapper
    {
        public void MapItemToProvisioningOrder(ORF_Order order, OrderItemContract orderItem, IEnumerable<OrderItemContract> childs)
        {
            var orfResellerPayU = new ORF_HostingPackage();
            orfResellerPayU.SetPackagePriceInformation(orderItem);
            order.HostingPackages.Add(orfResellerPayU);
        }

        public bool CanMap(OrderItemContract orderItem, Optional<UAC_ServicePackage> servicePackage)
        {
            return orderItem.ProductCode == "reseller-platform";
        }
    }
}
