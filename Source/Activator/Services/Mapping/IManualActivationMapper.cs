﻿using System.Collections.Generic;
using Combell.Model.OrderForm;
using Combell.Model.UAC;
using Intelligent.Shared.Core;
using Store.API.Contracts.Orders;

namespace Activator.Services.Mapping
{
    public interface IManualActivationMapper
    {
        void MapItemToProvisioningOrder(ORF_Order order, OrderItemContract orderItem, IEnumerable<OrderItemContract> childs);
        void MapDependingManualActivations(ORF_Order order, OrderItemContract parentItem, IEnumerable<OrderItemContract> childs, Optional<UAC_ServicePackage> servicePackage);
    }
}