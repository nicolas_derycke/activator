﻿using System.Linq;
using Combell.Model.UAC;
using Combell.Model.UAC.Enumerations;
using Combell.Model.UAC.ResourceSets;
using Intelligent.Shared.Core;

namespace Activator.Services.Mapping
{
    public static class ServicePackageExtensions
    {
        public static Optional<TResourceSet> GetActiveResourceSet<TResourceSet>(this Optional<UAC_ServicePackage> servicePackage, UAC_ServicePackageType servicePackageType)
            where TResourceSet: UAC_ResourceSet
        {
            if(!servicePackage.HasValue || servicePackage.Value.IsDisabled || servicePackage.Value.Type != servicePackageType)
                return Optional<TResourceSet>.Empty;

            return
                servicePackage.Value.ResourceSets
                    .OfType<UAC_HostingResourceSet>()
                    .Where(x => !x.DeActivated)
                    .SelectMany(x => x.ResourceSets.OfType<TResourceSet>())
                    .FirstOrDefault(x => !x.DeActivated);
        }
    }
}