﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Activator.Helpers;
using Combell;
using Combell.Model.OrderForm;
using Combell.Model.UAC;
using Intelligent.Shared.Core;
using Intelligent.Shared.Reflection;
using Store.API.Client;
using Store.API.Contracts;
using Store.API.Contracts.Orders;
using Store.API.Contracts.TypedItemAttributes;

namespace Activator.Services.Mapping
{
    public class ManualActivationMapper : IManualActivationMapper
    {
        private static readonly IEnumerable<AttributeKey> ParentKeysWithNoDpenedingManualActivationPossible = new[]
        {
            AttributeKey.Fax
        };

        private static readonly IEnumerable<string> ProductCodesThatAlwaysNeedToBeActivatedManually = new[]
        {
            "combell-mover"
        };

        public void MapItemToProvisioningOrder(ORF_Order order, OrderItemContract orderItem, IEnumerable<OrderItemContract> childs)
        {
            var manualActivation = CreateManualActivation(orderItem);

            foreach (var childItem in childs)
            {
                manualActivation.Childs.Add(CreateManualActivation(childItem));
            }

            order.ManualActivations.Add(manualActivation);
        }

        public void MapDependingManualActivations(ORF_Order order, OrderItemContract parentItem, IEnumerable<OrderItemContract> childs, Optional<UAC_ServicePackage> servicePackage )
        {
            foreach (var dependantChild in childs.Where(x =>
                (!x.Attributes.Any() ||
                 x.Attributes.ContainsKey(AttributeKey.ManualActivation) ||
                 ProductCodesThatAlwaysNeedToBeActivatedManually.Contains(x.ProductCode))
                && !parentItem.Attributes.Select(key => key.Key).Any(key => ParentKeysWithNoDpenedingManualActivationPossible.Contains(key))))
            {
                var manualActivation = CreateManualActivation(dependantChild);
                var identifierAttribute =
                    parentItem.Attributes
                        .Select(x => x.Value)
                        .Where(x => x.GetType().IsAssignableTo(typeof(IHaveAnIdentifierAttribute)))
                        .Cast<IHaveAnIdentifierAttribute>()
                        .FirstOrDefault();

                var identifier = identifierAttribute == null ? null : identifierAttribute.Identifier;
                manualActivation.Parent =
                    new ORF_ManualActivationParent
                    {
                        ProductId = parentItem.ProductId,
                        Identifier = identifier ?? dependantChild.Attributes.Identifier,
                        ServicePackageId = servicePackage.HasValue ? servicePackage.Value.ID : 0,
                        ProductName = parentItem.ProductName
                    };

                order.ManualActivations.Add(manualActivation);
            }
        }

        private static ORF_ManualActivation CreateManualActivation(OrderItemContract orderItem)
        {
            var manualActivation = new ORF_ManualActivation();
            manualActivation.SetPackagePriceInformation(orderItem);
            manualActivation.ProductCode = orderItem.ProductCode;
            manualActivation.Quantity = orderItem.Quantity;

            foreach (var attribute in orderItem.Attributes.GetOptionalBasketItemAttribute<ManualActivationAttributes>(AttributeKey.ManualActivation))
            {
                foreach (var manualActivationAttribute in attribute)
                {
                    manualActivation.Attributes.Add(new ORF_ManualActivationAttribute
                    {
                        Name = manualActivationAttribute.Name,
                        Value = manualActivationAttribute.Value
                    });
                }
            }

            return manualActivation;
        }
    }
}