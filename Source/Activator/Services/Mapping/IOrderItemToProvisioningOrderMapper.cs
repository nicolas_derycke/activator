﻿using System.Collections.Generic;
using Combell.Model.OrderForm;
using Combell.Model.UAC;
using Intelligent.Shared.Core;
using Store.API.Contracts.Orders;

namespace Activator.Services.Mapping
{
    public interface IOrderItemToProvisioningOrderMapper
    {
        bool CanMap(OrderItemContract orderItem, Optional<UAC_ServicePackage> servicePackage);
        void MapItemToProvisioningOrder(ORF_Order order, OrderItemContract orderItem, IEnumerable<OrderItemContract> childs);
    }
}