﻿using Activator.Helpers;
using Combell.Model.OrderForm;
using Combell.Model.UAC;
using Combell.Model.UAC.Enumerations;
using Combell.Model.UAC.ResourceSets;
using Intelligent.Shared.Core;
using Store.API.Client;
using Store.API.Contracts;
using Store.API.Contracts.Orders;
using Store.API.Contracts.Specifications;
using Store.API.Contracts.TypedItemAttributes;
using System.Collections.Generic;

namespace Activator.Services.Mapping
{
    public class UacAccountAddonMapper : IOrderItemToProvisioningOrderMapper
    {
        public void MapItemToProvisioningOrder(ORF_Order order, OrderItemContract orderItem, IEnumerable<OrderItemContract> childs)
        {
            var domainAttributes = orderItem.Attributes.GetBasketItemAttribute<DomainItemAttributes>(AttributeKey.Domain);
            var uacAccountAddonAttributes = orderItem.Attributes.GetBasketItemAttribute<UacAccountAddonAttributes>(AttributeKey.UacAccountAddon);

            if (uacAccountAddonAttributes.UpgradeFromAddonId.HasValue)
            {
                var upgradeUacAccountAddon = new ORF_UpgradeUacAccountAddon();
                upgradeUacAccountAddon.SetPackagePriceInformation(orderItem);

                upgradeUacAccountAddon.Identifier = domainAttributes.DomainName;
                upgradeUacAccountAddon.AccountId = uacAccountAddonAttributes.AccountId;
                upgradeUacAccountAddon.CurrentAddonId = uacAccountAddonAttributes.UpgradeFromAddonId.Value;

                order.UpgradeUacAccountAddons.Add(upgradeUacAccountAddon);
            }
            else
            {
                var uacAccountAddon = new ORF_UacAccountAddon();
                uacAccountAddon.SetPackagePriceInformation(orderItem);

                uacAccountAddon.Identifier = domainAttributes.DomainName;
                uacAccountAddon.AccountId = uacAccountAddonAttributes.AccountId;

                order.UacAccountAddons.Add(uacAccountAddon);
            }
        }

        public bool CanMap(OrderItemContract orderItem, Optional<UAC_ServicePackage> servicePackage)
        {
            return servicePackage.GetActiveResourceSet<UAC_LinuxHostingResourceSet>(UAC_ServicePackageType.AddOn).HasValue
                   && !orderItem.IsUniqueIp();
        }
    }
}