﻿using Activator.Helpers;
using Combell.Model.OrderForm;
using Combell.Model.UAC;
using Combell.Model.UAC.Enumerations;
using Combell.Model.UAC.ResourceSets;
using Intelligent.Shared.Core;
using Store.API.Client;
using Store.API.Contracts;
using Store.API.Contracts.Orders;
using Store.API.Contracts.TypedItemAttributes;
using System.Collections.Generic;
using System.Linq;

namespace Activator.Services.Mapping
{
    public class DomainMapper : IOrderItemToProvisioningOrderMapper
    {
        public void MapItemToProvisioningOrder(ORF_Order order, OrderItemContract orderItem, IEnumerable<OrderItemContract> childs)
        {
            var domainAttributes = orderItem.Attributes.GetBasketItemAttribute<DomainItemAttributes>(AttributeKey.Domain);
            var nameserversAttributes = orderItem.Attributes.GetOptionalBasketItemAttribute<NameserversAttributes>(AttributeKey.Nameservers);
            var registrantAttributes = orderItem.Attributes.GetOptionalBasketItemAttribute<RegistrantAttributes>(AttributeKey.Registrant);
            var transferDomainAttributes = orderItem.Attributes.GetOptionalBasketItemAttribute<TransferDomainAttributes>(AttributeKey.TransferDomain);

            string authCode = null;
            if (transferDomainAttributes.HasValue)
                authCode = transferDomainAttributes.Value.AuthorizationCode;
            var orfDomain = new ORF_Domain()
            {
                DomainName = domainAttributes.DomainName,
                DomainOrderReference = 1,
                AuthorizationCode = authCode,
                DomainAction = transferDomainAttributes.HasValue ? DomainActionTypes.Transfer : DomainActionTypes.Register,
                IsMainDomain = true
            };
            orfDomain.SetPackagePriceInformation(orderItem);

            var useExistingRegistrant = false;
            if (transferDomainAttributes.HasValue)
                useExistingRegistrant = transferDomainAttributes.Value.UseExistingRegistrant;
            if (registrantAttributes.HasValue && !useExistingRegistrant)
            {
                var registrant = registrantAttributes.Value;
                var orfDomainContact = new ORF_DomainContact()
                {
                    City = registrant.City,
                    Company = registrant.Company,
                    Country = registrant.Country.ToUpper(),
                    Email = registrant.Email,
                    Fax = registrant.Fax,
                    FirstName = registrant.FirstName,
                    GSM = registrant.Mobile,
                    HouseNumber = registrant.HouseNumber,
                    Language = registrant.Language,
                    Name = registrant.LastName,
                    PostalCode = registrant.PostalCode,
                    Street = registrant.Street,
                    Telephone = registrant.Telephone,
                    VatNumber = registrant.VatNumber
                };

                var extraFields = registrant.ExtraFields;
                if (extraFields != null)
                {
                    orfDomain.ExtraFields = new List<ORF_ExtraRegField>();
                    AddExtraField("CompanyNumber", extraFields.CompanyNumber, orfDomain);
                    AddExtraField("PassportNumber", extraFields.PassportNumber, orfDomain);
                    AddExtraField("PassportNumber", extraFields.CodiceFiscal, orfDomain);
                    AddExtraField("TrademarkName", extraFields.TrademarkName, orfDomain);
                    AddExtraField("TrademarkNumber", extraFields.TrademarkNumber, orfDomain);
                    AddExtraField("TrademarkCountry", extraFields.TrademarkCountry, orfDomain);
                }

                orfDomain.RegistrantContact = orfDomainContact;
            }

            if (transferDomainAttributes.HasValue && transferDomainAttributes.Value.UseExistingNameservers)
                orfDomain.NameServerAction = NameServerActionTypes.Existing;
            else if (nameserversAttributes.HasValue)
            {
                orfDomain.NameServerAction = NameServerActionTypes.OwnNameservers;
                orfDomain.NameServers = new List<ORF_NameServer>();
                var nameservers = nameserversAttributes.Value;
                foreach (var ns in nameservers)
                    orfDomain.NameServers.Add(
                        new ORF_NameServer()
                        {
                            Name = ns.Name,
                            Ip = ns.Ip
                        });
            }
            else
                orfDomain.NameServerAction = NameServerActionTypes.Forwarding;

            // add to order on the correct node
            if (order.HostingPackages.Any(hp => hp.Domains.ContainsDomainName(domainAttributes.DomainName)))
            {
                var hostingPackage = order.HostingPackages.First(hp => hp.Domains.ContainsDomainName(domainAttributes.DomainName));
                hostingPackage.Domains.SwapByDomainName(orfDomain);
            }
            else if (order.HostedExchangeDomainPackages.Any(hdp => hdp.Domains.ContainsDomainName(domainAttributes.DomainName)))
            {
                var hostedExchangeDomainPackage = order.HostedExchangeDomainPackages.First(hdp => hdp.Domains.ContainsDomainName(domainAttributes.DomainName));
                hostedExchangeDomainPackage.Domains.SwapByDomainName(orfDomain);
            }
            else
            {
                orfDomain.IsMainDomain = false;
                order.SingleDomains.Add(orfDomain);
            }
        }

        private static void AddExtraField(string fieldName, string value, ORF_Domain orfDomain)
        {
            if (!string.IsNullOrWhiteSpace(value))
                orfDomain.ExtraFields.Add(new ORF_ExtraRegField() { FieldName = fieldName, FieldValue = value });
        }

        public bool CanMap(OrderItemContract orderItem, Optional<UAC_ServicePackage> servicePackage)
        {
            return servicePackage.GetActiveResourceSet<UAC_DomainnamesResourceSet>(UAC_ServicePackageType.Basic).HasValue;
        }
    }
}
