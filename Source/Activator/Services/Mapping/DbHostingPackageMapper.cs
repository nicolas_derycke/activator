﻿using Activator.Helpers;
using Combell.Model.OrderForm;
using Combell.Model.UAC;
using Combell.Model.UAC.Enumerations;
using Combell.Model.UAC.ResourceSets;
using Intelligent.Shared.Core;
using Store.API.Client;
using Store.API.Contracts;
using Store.API.Contracts.Orders;
using Store.API.Contracts.TypedItemAttributes;
using System.Collections.Generic;

namespace Activator.Services.Mapping
{
    public class DbHostingPackageMapper : IOrderItemToProvisioningOrderMapper
    {
        public void MapItemToProvisioningOrder(ORF_Order order, OrderItemContract orderItem, IEnumerable<OrderItemContract> childs)
        {

            var domainAttribute = orderItem.Attributes.GetOptionalBasketItemAttribute<DomainItemAttributes>(AttributeKey.Domain);
            var databaseAttribute = orderItem.Attributes.GetBasketItemAttribute<DatabaseItemAttribute>(AttributeKey.Database);

            var dbHostingPackage = new ORF_DatabasePackage();

            if (domainAttribute.HasValue)
            {
                if (!databaseAttribute.IsStandalone)
                {
                    dbHostingPackage.DomainName = domainAttribute.Value.DomainName;
                }
            }

            if (!string.IsNullOrEmpty(databaseAttribute.UpgradeFromDatabaseName))
            {
                order.Remark = "ProductOptionEntity=" + databaseAttribute.UpgradeFromDatabaseName;
            }

            dbHostingPackage.SetPackagePriceInformation(orderItem);

            for (int i = 0; i < orderItem.Quantity; i++)
            {
                order.DbHostingPackages.Add(dbHostingPackage);
            }
        }

        public bool CanMap(OrderItemContract orderItem, Optional<UAC_ServicePackage> servicePackage)
        {
            var mySqlDbHostingResourceSet =
                servicePackage.GetActiveResourceSet<UAC_MySqlDatabaseResourceSet>(UAC_ServicePackageType.Basic);
            var msSqlDbHostingResourceSet =
                servicePackage.GetActiveResourceSet<UAC_SqlServerDatabaseResourceSet>(UAC_ServicePackageType.Basic);

            return mySqlDbHostingResourceSet.HasValue || msSqlDbHostingResourceSet.HasValue;
        }
    }
}
