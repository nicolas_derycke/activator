﻿using Activator.Helpers;
using Combell.Model.OrderForm;
using Combell.Model.UAC;
using Combell.Model.UAC.Enumerations;
using Combell.Model.UAC.ResourceSets;
using Intelligent.Shared.Core;
using Store.API.Client;
using Store.API.Contracts;
using Store.API.Contracts.Orders;
using Store.API.Contracts.Specifications;
using Store.API.Contracts.TypedItemAttributes;
using System.Collections.Generic;

namespace Activator.Services.Mapping
{
    public class HostingPackageMapper : IOrderItemToProvisioningOrderMapper
    {
        public void MapItemToProvisioningOrder(ORF_Order order, OrderItemContract orderItem, IEnumerable<OrderItemContract> childs)
        {
            var domainAttributes = orderItem.Attributes.GetBasketItemAttribute<DomainItemAttributes>(AttributeKey.Domain);
            var hostingAttributes = orderItem.Attributes.GetOptionalBasketItemAttribute<HostingAttributes>(AttributeKey.Hosting);

            if (hostingAttributes.HasValue &&
                hostingAttributes.Value.UpgradeFromAccountId.HasValue)
            {
                var upgradeFromAccountId = hostingAttributes.Value.UpgradeFromAccountId.Value;

                var upgradeHostingPackage = new ORF_UpgradeHostingPackage();
                upgradeHostingPackage.SetPackagePriceInformation(orderItem);

                upgradeHostingPackage.Identifier = domainAttributes.DomainName;
                upgradeHostingPackage.AccountId = upgradeFromAccountId;

                order.UpgradeHostingPackages.Add(upgradeHostingPackage);
            }
            else
            {
                var orfHostingPackage = new ORF_HostingPackage();
                orfHostingPackage.SetPackagePriceInformation(orderItem);

                var orfHostingPackageDomain = OrfOrderExtensions.CreateNoActionOrfDomain(domainAttributes.DomainName);

                orfHostingPackage.Domains.Add(orfHostingPackageDomain);

                order.HostingPackages.Add(orfHostingPackage);
            }
        }

        public bool CanMap(OrderItemContract orderItem, Optional<UAC_ServicePackage> servicePackage)
        {
            var linuxHostingResourceSet = servicePackage.GetActiveResourceSet<UAC_LinuxHostingResourceSet>(UAC_ServicePackageType.Basic);
            var windowsHostingResourceSet = servicePackage.GetActiveResourceSet<UAC_WindowsHostingResourceSet>(UAC_ServicePackageType.Basic);
            var websplanetResourceSet = servicePackage.GetActiveResourceSet<UAC_WebsplanetResourceSet>(UAC_ServicePackageType.Basic);

            return ((linuxHostingResourceSet.HasValue && string.IsNullOrWhiteSpace(linuxHostingResourceSet.Value.Application)) ||
                   windowsHostingResourceSet.HasValue ||
                   websplanetResourceSet.HasValue) &&
                   !orderItem.IsUniqueIp();
        }
    }
}
