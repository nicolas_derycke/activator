using System.Threading.Tasks;
using Activator.Messages;

namespace Activator.Services.Activators
{
    public interface IOrderActivator
    {
        Task ActivateOrder(
            string orderCode,
            ActivationMethod activationMethod,
            string activationReason,
            int activationById,
            string activationBy);
    }
}