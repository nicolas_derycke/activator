﻿using Activator.Messages;
using Activator.Messages.Events;
using Activator.Proxies;
using Intelligent.Core.Models;
using MassTransit;
using Serilog;
using Store.API.Contracts;
using Store.API.Contracts.Orders;
using Store.API.Contracts.Specifications;
using System;
using System.Linq;
using System.Runtime.ExceptionServices;
using System.Threading.Tasks;

namespace Activator.Services.Activators
{
    public class OrderActivator : IOrderActivator
    {
        private readonly IStoreApiProxy _storeApiProxy;
        private readonly IBus _bus;
        private readonly IProvisioningOrderActivator _provisioningOrderActivator;
        private readonly IPreRegistrationActivator _preRegistrationActivator;
        private readonly ILogger _logger;

        public OrderActivator(
            IStoreApiProxy storeApiProxy,
            IBus bus,
            IProvisioningOrderActivator provisioningOrderActivator,
            IPreRegistrationActivator preRegistrationActivator,
            ILogger logger)
        {
            if (storeApiProxy == null) throw new ArgumentNullException("storeApiProxy");
            if (bus == null) throw new ArgumentNullException("bus");
            if (provisioningOrderActivator == null) throw new ArgumentNullException("provisioningOrderActivator");
            if (preRegistrationActivator == null) throw new ArgumentNullException("preRegistrationActivator");
            if (logger == null) throw new ArgumentNullException("logger");

            _storeApiProxy = storeApiProxy;
            _bus = bus;
            _provisioningOrderActivator = provisioningOrderActivator;
            _preRegistrationActivator = preRegistrationActivator;
            _logger = logger;
        }

        public async Task ActivateOrder(
            string orderCode,
            ActivationMethod activationMethod,
            string activationReason,
            int activationById,
            string activationBy)
        {
            _logger.Information("Activating order '{orderCode}'", orderCode);

            ExceptionDispatchInfo exception = null;
            try
            {
                //Add delay because activateorder sometimes happens to fast.
                await Task.Delay(TimeSpan.FromSeconds(3));

                var order = await _storeApiProxy.GetOrder(orderCode);
                var customerIdentifier = CustomerIdentifier.FromCustomerNumber(order.CustomerNumber);
                
                var orderState = await _storeApiProxy.GetOrderState(orderCode);
                if (orderState.ActivationState.Status == OrderActivationStatusContract.Activated)
                {
                    _logger.Information("Ignoring order {orderCode} because it was already activated", orderCode);
                    return;
                }

                if (order.Status == OrderStatus.Cancelled)
                {
                    _logger.Information("Ignoring order {orderCode} beacause it has been cancelled", orderCode);
                    return;
                }

                var provisioningOrderCodes = await _provisioningOrderActivator.Activate(customerIdentifier, order);
                await _preRegistrationActivator.Activate(customerIdentifier, order.OrderCode, order.Items.Where(x => x.IsPreRegistration()));

                await _bus.Publish(
                    new OrderActivated(
                        orderCode, 
                        activationMethod, 
                        activationReason, 
                        activationById, 
                        activationBy,
                        provisioningOrderCodes, 
                        DateTime.Now));

                _logger.Information("Activated order '{orderCode}'", orderCode);
            }
            catch (Exception ex)
            {
                exception = ExceptionDispatchInfo.Capture(ex);
            }

            if (exception != null)
            {
                await _bus.Publish(new OrderActivationFailed(orderCode, DateTime.Now));
                exception.Throw();
            }
        }
    }
}