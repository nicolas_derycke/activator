﻿using Activator.Proxies;
using Activator.Queries;
using Activator.Queries.Data;
using Intelligent.Core.Models;
using Intelligent.Shared.Core;
using Intelligent.Shared.Core.Queries;
using Store.API.Contracts.Orders;
using Store.API.Contracts.Specifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Threading.Tasks;

namespace Activator.Services.Activators
{
    public class ProvisioningOrderActivator : IProvisioningOrderActivator
    {
        private readonly IProvisioningOrderServiceProxy _provisioningOrderServiceProxy;
        private readonly IOrderContractToOrfOrderMapper _orderContractToOrfOrderMapper;
        private readonly IQueryHandler _queryHandler;

        public ProvisioningOrderActivator(
            IProvisioningOrderServiceProxy provisioningOrderServiceProxy,
            IOrderContractToOrfOrderMapper orderContractToOrfOrderMapper,
            IQueryHandler queryHandler)
        {
            if (provisioningOrderServiceProxy == null) throw new ArgumentNullException("provisioningOrderServiceProxy");
            if (orderContractToOrfOrderMapper == null) throw new ArgumentNullException("orderContractToOrfOrderMapper");
            if (queryHandler == null) throw new ArgumentNullException("queryHandler");

            this._provisioningOrderServiceProxy = provisioningOrderServiceProxy;
            this._orderContractToOrfOrderMapper = orderContractToOrfOrderMapper;
            this._queryHandler = queryHandler;
        }

        public async Task<IEnumerable<string>> Activate(CustomerIdentifier customerIdentifier, OrderContract order)
        {
            if (order.Items.All(x => x.IsPreRegistration()))
                return Optional<string>.Empty;

            var provisioningOrderStatus = _queryHandler.Execute(new GetProvisioningOrderStatusQuery(order.OrderCode, customerIdentifier));
            if (provisioningOrderStatus.HasValue && provisioningOrderStatus.Value != ProvisioningOrderStatus.Initializing)
                return new Optional<string>(order.OrderCode);

            var orfOrder = await _orderContractToOrfOrderMapper.Map(customerIdentifier, order);
            orfOrder.IpAddress = GetIpAddress();
            _provisioningOrderServiceProxy.SubmitOrder(orfOrder);

            return new Optional<string>(order.OrderCode);
        }

        private string GetIpAddress()
        {
            var intelliProperties =
                (IEnumerable<KeyValuePair<string, string>>) CallContext.LogicalGetData("IntelliProperties");
            if (intelliProperties == null)
                return null;

            return intelliProperties.FirstOrDefault(p => p.Key == "IntelliIpAddress").Value;            
        }
    }
}