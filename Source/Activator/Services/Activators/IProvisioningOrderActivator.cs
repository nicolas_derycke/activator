using System.Collections.Generic;
using System.Threading.Tasks;
using Intelligent.Core.Models;
using Store.API.Contracts.Orders;

namespace Activator.Services.Activators
{
    public interface IProvisioningOrderActivator
    {
        Task<IEnumerable<string>> Activate(CustomerIdentifier customerIdentifier, OrderContract order);
    }
}