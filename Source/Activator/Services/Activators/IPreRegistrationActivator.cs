using System.Collections.Generic;
using System.Threading.Tasks;
using Intelligent.Core.Models;
using Store.API.Contracts.Orders;

namespace Activator.Services.Activators
{
    public interface IPreRegistrationActivator
    {
        Task Activate(CustomerIdentifier customerIdentifier, string orderCode, IEnumerable<OrderItemContract> preRegistrations);
    }
}