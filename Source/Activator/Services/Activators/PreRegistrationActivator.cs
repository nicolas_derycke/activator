﻿using Activator.Commands;
using Activator.Helpers;
using Activator.Proxies;
using Activator.Queries;
using Core.Templating.API.Client;
using Customer.API.Contracts;
using Intelligent.Core.Messages.Commands;
using Intelligent.Core.Models;
using Intelligent.Shared.Core.Commands;
using Intelligent.Shared.Core.Queries;
using MassTransit;
using Store.API.Client;
using Store.API.Contracts;
using Store.API.Contracts.Orders;
using Store.API.Contracts.TypedItemAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace Activator.Services.Activators
{
    public class PreRegistrationActivator : IPreRegistrationActivator
    {
        private readonly IQueryHandler _queryHandler;
        private readonly ICommandHandler _commandHandler;
        private readonly IBus _bus;
        private readonly ITemplateClient _templateClient;
        private readonly ICustomerApiProxy _customerApiProxy;

        public PreRegistrationActivator(
            IQueryHandler queryHandler,
            ICommandHandler commandHandler,
            IBus bus,
            ITemplateClient templateClient,
            ICustomerApiProxy customerApiProxy)
        {
            if (queryHandler == null) throw new ArgumentNullException("queryHandler");
            if (commandHandler == null) throw new ArgumentNullException("commandHandler");
            if (bus == null) throw new ArgumentNullException("bus");
            if (templateClient == null) throw new ArgumentNullException("templateClient");
            if (customerApiProxy == null) throw new ArgumentNullException("customerApiProxy");

            this._queryHandler = queryHandler;
            this._commandHandler = commandHandler;
            this._bus = bus;
            this._templateClient = templateClient;
            this._customerApiProxy = customerApiProxy;
        }

        public async Task Activate(CustomerIdentifier customerIdentifier, string orderCode, IEnumerable<OrderItemContract> preregistrations)
        {
            if (!preregistrations.Any())
                return;

            var customer = await _customerApiProxy.Get(customerIdentifier);
            var provider = new Provider((ProviderValue)Enum.Parse(typeof(ProviderValue), customer.Provider, true));
            var primaryContact = customer.GetContactData(ContactTypeContract.Primary);

            var infoPreregistrations = new List<OrderItemContract>();
            var eapPreregistrations = new List<OrderItemContract>();
            foreach(var preregistration in preregistrations)
            {
                var isGoLive = false;
                var isLandRush = false;
                var optionalBasketItemAttribute = preregistration.Attributes.GetOptionalBasketItemAttribute<PreregistrationAttributes>(AttributeKey.Preregistration);
                if(optionalBasketItemAttribute.HasValue)
                {
                    isGoLive = optionalBasketItemAttribute.Value.IsGoLive;
                    isLandRush = optionalBasketItemAttribute.Value.IsLandRush;
                }

                if (isGoLive || isLandRush)
                    eapPreregistrations.Add(preregistration);
                else
                    infoPreregistrations.Add(preregistration);
            }

            if(infoPreregistrations.Count() > 0)
                await SendInfoEmail(customerIdentifier, orderCode, infoPreregistrations, customer, provider, primaryContact);
            if(eapPreregistrations.Count() > 0)
                await SendEapEmail(customerIdentifier, orderCode, eapPreregistrations, customer, provider, primaryContact);
        }

        private async Task SendInfoEmail(
            CustomerIdentifier customerIdentifier,
            string orderCode,
            IEnumerable<OrderItemContract> preRegistrations,
            CustomerContract customer,
            Provider provider,
            ContactData primaryContact)
        {
            var template = await _templateClient.GetTemplate("@@order_by_existing_customer", provider, primaryContact.Language);
            var singleDomainRow = await _templateClient.GetTemplate("@@gtld_domain", provider, primaryContact.Language);
            var otherCustomerPreRegisteredDomainWarning = await _templateClient.GetTemplate("@@gtld_existingdomains", provider, primaryContact.Language);

            var gtldOrders = new StringBuilder();
            var existingDomains = string.Empty;
            using (var transaction = new TransactionScope())
            {
                foreach (var preRegistration in preRegistrations)
                {
                    var domainAttributes = preRegistration.Attributes.GetBasketItemAttribute<DomainItemAttributes>(AttributeKey.Domain);
                    var domainName = new DomainName(domainAttributes.DomainName);

                    var gtldDomains = _queryHandler.Execute(new GetGtldDomainsByNameQuery(domainName));
                    if (!gtldDomains.Any(d => d.CustomerId == customerIdentifier.CustomerId))
                        _commandHandler.Execute(new CreateGtldDomainCommand(customerIdentifier, domainName, false, new DateTime(1900, 1, 1), false, new DateTime(1900, 1, 1)));
                    
                    if (gtldDomains.Any(d => d.CustomerId != customerIdentifier.CustomerId))
                        existingDomains = otherCustomerPreRegisteredDomainWarning.Body;

                    var gtldOrder = singleDomainRow.Body;
                    gtldOrder = gtldOrder.Replace("$$DOMAIN$$", domainName.FullName);
                    gtldOrder = gtldOrder.Replace("$$TYPE$$", "Info");
                    gtldOrders.Append(gtldOrder);
                }

                template.Body = template.Body.Replace("$$PRIMARYFIRSTNAME$$", primaryContact.FirstName);
                template.Body = template.Body.Replace("$$PRIMARYLASTNAME$$", primaryContact.LastName);
                template.Body = template.Body.Replace("$$GTLD_ORDERS$$", gtldOrders.ToString());
                template.Body = template.Body.Replace("$$EXISTINGDOMAINS$$", existingDomains);
                template.Body = template.Body.Replace("$$CUSTOMERNUMBER$$", customerIdentifier.CustomerNumber.ToString());
                template.Body = template.Body.Replace("$$COMPANY$$", customer.CompanyName);
                template.Body = template.Body.Replace("$$PRIMARYEMAIL$$", primaryContact.Email);

                template.Subject += " - " + orderCode;
                
                var command = new SendEmailCommand(
                    template.From,
                    primaryContact.Email,
                    string.Empty,
                    string.Empty,
                    template.Subject,
                    template.Body,
                    customerIdentifier.CustomerId);
                command.Ordercode = orderCode;
                command.Description = "Pre-registration activation";

                await _bus.Publish(command);

                transaction.Complete();
            }
        }

        private async Task SendEapEmail(
            CustomerIdentifier customerIdentifier,
            string orderCode,
            IEnumerable<OrderItemContract> preRegistrations,
            CustomerContract customer,
            Provider provider,
            ContactData primaryContact)
        {
            var template = await _templateClient.GetTemplate("@@order_by_existing_customer_eap", provider, primaryContact.Language);
            var singleDomainRow = await _templateClient.GetTemplate("@@gtld_domain", provider, primaryContact.Language);
            var otherCustomerPreRegisteredDomainWarning = await _templateClient.GetTemplate("@@gtld_existingdomains", provider, primaryContact.Language);

            var extension = "";
            var gtldOrders = new StringBuilder();
            using (var transaction = new TransactionScope())
            {
                foreach (var preRegistration in preRegistrations)
                {
                    var domainAttributes = preRegistration.Attributes.GetBasketItemAttribute<DomainItemAttributes>(AttributeKey.Domain);
                    var domainName = new DomainName(domainAttributes.DomainName);
                    extension = domainName.Extension.Name.ToUpper();

                    var gtldOrder = singleDomainRow.Body;
                    var queuedForLandrushDate = new DateTime(1900, 1, 1);
                    var queuedForGoliveDate = new DateTime(1900, 1, 1);
                    var preregistrationAttributes = preRegistration.Attributes.GetBasketItemAttribute<PreregistrationAttributes>(AttributeKey.Preregistration);
                    if (preregistrationAttributes.IsGoLive)
                    {
                        queuedForGoliveDate = preregistrationAttributes.StageDate;
                        gtldOrder = gtldOrder.Replace("$$TYPE$$", "GoLive");
                    }
                    else if (preregistrationAttributes.IsLandRush)
                    {
                        queuedForLandrushDate = preregistrationAttributes.StageDate;
                        gtldOrder = gtldOrder.Replace("$$TYPE$$", "LandRush");
                    }

                    _commandHandler.Execute(new CreateGtldDomainCommand(
                        customerIdentifier,
                        domainName,
                        preregistrationAttributes.IsLandRush,
                        queuedForLandrushDate,
                        preregistrationAttributes.IsGoLive,
                        queuedForGoliveDate));

                    gtldOrder = gtldOrder.Replace("$$DOMAIN$$", domainName.FullName);
                    gtldOrders.Append(gtldOrder);
                }

                template.Body = template.Body.Replace("$$PRIMARYFIRSTNAME$$", primaryContact.FirstName);
                template.Body = template.Body.Replace("$$PRIMARYLASTNAME$$", primaryContact.LastName);
                template.Body = template.Body.Replace("$$GTLD_ORDERS$$", gtldOrders.ToString());
                template.Body = template.Body.Replace("$$CUSTOMERNUMBER$$", customerIdentifier.CustomerNumber.ToString());
                template.Body = template.Body.Replace("$$COMPANY$$", customer.CompanyName);
                template.Body = template.Body.Replace("$$PRIMARYEMAIL$$", primaryContact.Email);
                template.Body = template.Body.Replace("$$GTLDEXTENSION$$", extension);

                template.Subject += " - " + orderCode;
                template.Subject = template.Subject.Replace("$$GTLDEXTENSION$$", extension);

                var command = new SendEmailCommand(
                    template.From,
                    primaryContact.Email,
                    string.Empty,
                    string.Empty,
                    template.Subject,
                    template.Body,
                    customerIdentifier.CustomerId);
                command.Ordercode = orderCode;
                command.Description = "Pre-registration activation eap";

                await _bus.Publish(command);

                transaction.Complete();
            }
        }
    }
}