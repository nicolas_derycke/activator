using System;
using Combell.BusinessLogic.WebService.Contracts;
using Combell.Model.UAC;
using Intelligent.Core.Models;
using Intelligent.Shared.Core;
using Intelligent.Shared.ServiceModel.Client;

namespace Activator.Services
{
    public class ServicePackageProvider : IServicePackageProvider
    {
        private readonly IServiceInvoker<IBLWS_UAC> _uacServiceProxy;

        public ServicePackageProvider(IServiceInvoker<IBLWS_UAC> uacServiceProxy)
        {
            if (uacServiceProxy == null) throw new ArgumentNullException("uacServiceProxy");
            _uacServiceProxy = uacServiceProxy;
        }

        public Optional<UAC_ServicePackage> GetServicePackage(int productId, int providerId)
        {
            var provider = new Provider(providerId);
            var servicePackage = _uacServiceProxy.Invoke(x => x.UAC_GetServicePackageByProductID(productId, provider.ResellerId));
            return servicePackage ?? Optional<UAC_ServicePackage>.Empty;
        }
    }
}