﻿using System.Data;
using System.Linq;
using Activator.Queries.Data;
using Dapper;
using Intelligent.Core.Models;
using Intelligent.Shared.Core;
using Intelligent.Shared.Core.Queries;
using Intelligent.Shared.Data;

namespace Activator.Queries
{
    public class GetProvisioningOrderStatusQuery : IQuery<Optional<ProvisioningOrderStatus>>
    {
        public string OrderCode { get; private set; }
        public CustomerIdentifier CustomerIdentifier { get; private set; }

        public GetProvisioningOrderStatusQuery(string orderCode, CustomerIdentifier customerIdentifier)
        {
            this.OrderCode = orderCode;
            this.CustomerIdentifier = customerIdentifier;
        }

        public Optional<ProvisioningOrderStatus> Execute()
        {
            dynamic order;

            using (var connection = SqlConnectionFactory.CreateCombellGroupConnection())
                order = connection.Query<dynamic>(
                    "[dbo].[ORQ_GetOrderStatusOverview]",
                    new
                    {
                        OrderCode,
                        CustomerIdentifier.CustomerId
                    },
                    commandType: CommandType.StoredProcedure).FirstOrDefault();

            if (order == null)
                return Optional<ProvisioningOrderStatus>.Empty;

            return (ProvisioningOrderStatus)order.OrderQueueStatusID;
        }
    }
}