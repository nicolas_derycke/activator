﻿using System;

namespace Activator.Queries.Data
{
    public class GtldDomain
    {
        public int DomainId { get; private set; }
        public int CustomerId { get; private set; }
        public string DomainName { get; private set; }
        public DateTime QueuedForLandrushDate { get; private set; }
        public DateTime QueuedForGoliveDate { get; private set; }

        public GtldDomain(int domainId, int customerId, string domainName, DateTime queuedForLandrushDate, DateTime QueuedForGoliveDate)
        {
            this.DomainId = domainId;
            this.CustomerId = customerId;
            this.DomainName = domainName;
            this.QueuedForLandrushDate = QueuedForLandrushDate;
            this.QueuedForGoliveDate = QueuedForGoliveDate;
        }
    }
}