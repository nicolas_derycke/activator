﻿namespace Activator.Queries.Data
{
    public enum ProvisioningOrderStatus
    {
        Initializing = 10,
        ReadyForProcessing = 20,
        Processing = 30,
        Pending = 35,
        Error = 40,
        Finished = 50,
        Canceled = 60,
        CanceledDeleteProforma = 70,
        FailedValidation = 80
    }
}