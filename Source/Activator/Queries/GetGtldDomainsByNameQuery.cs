﻿using Activator.Queries.Data;
using Dapper;
using Intelligent.Core.Models;
using Intelligent.Shared.Core.Queries;
using Intelligent.Shared.Data;
using System.Collections.Generic;
using System.Data;

namespace Activator.Queries
{
    public class GetGtldDomainsByNameQuery : IQuery<IEnumerable<GtldDomain>>
    {
        public DomainName DomainName { get; private set; }

        public GetGtldDomainsByNameQuery(DomainName domainName)
        {
            this.DomainName = domainName;
        }

        public IEnumerable<GtldDomain> Execute()
        {
            using (var connection = SqlConnectionFactory.CreateCombellGroupConnection())
                return connection.Query<GtldDomain>(
                    "[dbo].[GTLD_GetDomainsByName]",
                    new
                    {
                        DomainName = DomainName.FullName
                    },
                    commandType: CommandType.StoredProcedure);
        }
    }
}