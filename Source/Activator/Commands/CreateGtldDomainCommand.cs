﻿using Dapper;
using Intelligent.Core.Models;
using Intelligent.Shared.Core.Commands;
using Intelligent.Shared.Data;
using System;
using System.Data;

namespace Activator.Commands
{
    public class CreateGtldDomainCommand : ICommand
    {
        public CustomerIdentifier CustomerIdentifier { get; private set; }
        public DomainName DomainName { get; private set; }
        public bool QueuedForLandrush { get; private set; }
        public DateTime QueuedForLandrushDate { get; private set; }
        public bool QueuedForGolive { get; private set; }
        public DateTime QueuedForGoliveDate { get; private set; }

        public CreateGtldDomainCommand(CustomerIdentifier customerIdentifier, DomainName domainName, bool queuedForLandrush, DateTime queuedForLandrushDate, bool queuedForGolive, DateTime queuedForGoliveDate)
        {
            CustomerIdentifier = customerIdentifier;
            DomainName = domainName;
            QueuedForLandrush = queuedForLandrush;
            QueuedForLandrushDate = queuedForLandrushDate;
            QueuedForGolive = queuedForGolive;
            QueuedForGoliveDate = queuedForGoliveDate;
        }

        public void Execute()
        {
            using (var connection = SqlConnectionFactory.CreateCombellGroupConnection())
                connection.Execute(
                    "[dbo].[GTLD_DomainAdd]",
                    new
                    {
                        CustomerIdentifier.CustomerId,
                        DomainName = DomainName.Name,
                        DomainExtension = DomainName.Extension.Name,
                        PhpDomainExtensionID = 0,
                        QueuedForLandrush = QueuedForLandrush,
                        QueuedForLandrushDate = QueuedForLandrushDate,
                        QueuedForGolive = QueuedForGolive,
                        QueuedForGoliveDate = QueuedForGoliveDate
                    },
                    commandType: CommandType.StoredProcedure);
        }
    }
}