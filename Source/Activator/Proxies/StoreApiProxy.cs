﻿using System;
using System.Configuration;
using System.Threading.Tasks;
using Core.Security;
using Intelligent.Shared.Net.Http;
using Store.API.Contracts.Orders;

namespace Activator.Proxies
{
    public class StoreApiProxy : IStoreApiProxy
    {
        private readonly ITokenProvider _tokenProvider;

        public StoreApiProxy(ITokenProvider tokenProvider)
        {
            if (tokenProvider == null) throw new ArgumentNullException("tokenProvider");

            this._tokenProvider = tokenProvider;
        }

        public async Task<OrderContract> GetOrder(string orderCode)
        {
            var client = StronglyTypedHttpClient.Create().WithBearerToken(_tokenProvider.GetApplicationGrantToken("orderdomain.activator").AccessToken).Build();
            var url = string.Format("{0}orders/{1}", ConfigurationManager.AppSettings["StoreApiUrl"], orderCode);
            return await client.GetAsync<OrderContract>(url);
        }

        public async Task<OrderStateContract> GetOrderState(string orderCode)
        {
            var client = StronglyTypedHttpClient.Create().WithBearerToken(_tokenProvider.GetApplicationGrantToken("orderdomain.activator").AccessToken).Build();
            var url = string.Format("{0}orderstates/{1}", ConfigurationManager.AppSettings["StoreApiUrl"], orderCode);
            return await client.GetAsync<OrderStateContract>(url);
        }
    }
}