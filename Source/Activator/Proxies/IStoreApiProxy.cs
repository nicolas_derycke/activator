using System.Threading.Tasks;
using Store.API.Contracts.Orders;

namespace Activator.Proxies
{
    public interface IStoreApiProxy
    {
        Task<OrderContract> GetOrder(string orderCode);
        Task<OrderStateContract> GetOrderState(string orderCode);
    }
}