﻿using System;
using Billing.API.Common.Interface.Contracts.Data;
using Billing.API.Common.Interface.Contracts.Requests;
using Billing.API.Common.Interface.Contracts.Services;
using Intelligent.Core.Models;
using Intelligent.Shared.ServiceModel.Client;

namespace Activator.Proxies
{
    public class CustomerServiceProxy : ICustomerServiceProxy
    {
        private readonly IServiceInvoker<ICustomerService> _customerService;

        public CustomerServiceProxy(IServiceInvoker<ICustomerService> customerService)
        {
            if (customerService == null) throw new ArgumentNullException("customerService");

            this._customerService = customerService;
        }

        public CustomerInvoiceCriteriaDataContract GetCustomerInvoiceCriteria(CustomerIdentifier customerIdentifier)
        {
            return _customerService.Invoke(cs => cs.GetCustomerInvoiceCriteria(new GetCustomerInvoiceCriteriaRequest(customerIdentifier.CustomerId)));
        }
    }
}