using Billing.API.Common.Interface.Contracts.Data;
using Intelligent.Core.Models;

namespace Activator.Proxies
{
    public interface ICustomerServiceProxy
    {
        CustomerInvoiceCriteriaDataContract GetCustomerInvoiceCriteria(CustomerIdentifier customerIdentifier);
    }
}