﻿using System;
using System.Configuration;
using System.Threading.Tasks;
using Core.Security;
using Customer.API.Contracts;
using Intelligent.Core.Models;
using Intelligent.Shared.Net.Http;

namespace Activator.Proxies
{
    public class CustomerApiProxy : ICustomerApiProxy
    {
        private readonly ITokenProvider _tokenProvider;

        public CustomerApiProxy(ITokenProvider tokenProvider)
        {
            if (tokenProvider == null) throw new ArgumentNullException("tokenProvider");

            this._tokenProvider = tokenProvider;
        }

        public async Task<CustomerContract> Get(CustomerIdentifier customerIdentifier)
        {
            var client = StronglyTypedHttpClient.Create().WithBearerToken(_tokenProvider.GetApplicationGrantToken("orderdomain.activator").AccessToken).Build();
            var url = string.Format("{0}customers/{1}", ConfigurationManager.AppSettings["CustomerApiUrl"], customerIdentifier.CustomerNumber);
            return await client.GetAsync<CustomerContract>(url);
        }
    }
}
