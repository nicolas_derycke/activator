using System.Threading.Tasks;
using Customer.API.Contracts;
using Intelligent.Core.Models;

namespace Activator.Proxies
{
    public interface ICustomerApiProxy
    {
        Task<CustomerContract> Get(CustomerIdentifier customerIdentifier);
    }
}