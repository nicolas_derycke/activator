﻿using System;
using Combell.BusinessLogic.WebService.Contracts;
using Combell.Model.OrderForm;
using Intelligent.Shared.ServiceModel.Client;

namespace Activator.Proxies
{
    public class ProvisioningOrderServiceProxy : IProvisioningOrderServiceProxy
    {
        private readonly IServiceInvoker<IBLWS_OrderForm> _orderFormService;

        public ProvisioningOrderServiceProxy(IServiceInvoker<IBLWS_OrderForm> orderFormService)
        {
            if (orderFormService == null) throw new ArgumentNullException("orderFormService");

            this._orderFormService = orderFormService;
        }

        public void SubmitOrder(ORF_Order order)
        {
            _orderFormService.Invoke(of => of.SubmitOrder(order));
        }
    }
}