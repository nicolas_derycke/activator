using Combell.Model.OrderForm;

namespace Activator.Proxies
{
    public interface IProvisioningOrderServiceProxy
    {
        void SubmitOrder(ORF_Order order);
    }
}