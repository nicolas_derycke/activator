﻿using Activator.Messages;
using Activator.Messages.Commands;
using Billing.Messages;
using MassTransit;
using Serilog;
using System;
using System.Threading.Tasks;

namespace Activator.Service.EventHandlers
{
    public class OrderPaidHandler : IConsumer<OrderPaid>
    {
        private readonly ILogger _logger;

        public OrderPaidHandler(ILogger logger)
        {
            if (logger == null) throw new ArgumentNullException("logger");
            this._logger = logger;
        }

        public async Task Consume(ConsumeContext<OrderPaid> context)
        {
            var orderCode = context.Message.OrderCode;

            if (string.IsNullOrWhiteSpace(orderCode))
            {
                _logger.Information("Order not activated as no order code is present");
                return;
            }

            await context.Publish(new ActivateOrder(orderCode, ActivationMethod.Payment, "Order is paid", 6, "System"));
            _logger.Information("Activate order command published for {orderCode}", orderCode);
        }
    }
}