﻿using Activator.Messages.Commands;
using Billing.Messages;
using Intelligent.Core.Users;
using MassTransit;
using Serilog;
using System;
using System.Threading.Tasks;
using Activator.Messages;

namespace Activator.Service.EventHandlers
{
    public class OrderInvoicedHandler : IConsumer<OrderInvoiced>
    {
        private readonly ILogger _logger;

        public OrderInvoicedHandler(ILogger logger)
        {
            if (logger == null) throw new ArgumentNullException("logger");
            this._logger = logger;
        }

        public async Task Consume(ConsumeContext<OrderInvoiced> context)
        {
            var orderInvoiced = context.Message;

            var orderCode = orderInvoiced.OrderCode;

            if (orderInvoiced.TotalAmountInclVat == 0
                || orderInvoiced.InvoiceType == InvoiceType.PeriodFreeProforma
                || orderInvoiced.InvoiceType == InvoiceType.PermanentFreeProforma)
            {
                await context.Publish(new ActivateOrder(orderCode, ActivationMethod.Free, "Order is free or invoiced on free proforma.", SystemUser.Id, SystemUser.Name));
                _logger.Information("Activate order command published for {orderCode}", orderCode);
            }
        }
    }
}