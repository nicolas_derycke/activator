﻿using Activator.Messages;
using Activator.Messages.Commands;
using Activator.Proxies;
using Intelligent.Core.Models;
using MassTransit;
using Serilog;
using Store.Messages.Events;
using System;
using System.Threading.Tasks;

namespace Activator.Service.EventHandlers
{
    public class OrderCreatedHandler : IConsumer<OrderCreated>
    {
        private readonly ICustomerServiceProxy _customerServiceProxy;
        private readonly ILogger _logger;

        public OrderCreatedHandler(ICustomerServiceProxy customerServiceProxy, ILogger logger)
        {
            if (customerServiceProxy == null) throw new ArgumentNullException("customerServiceProxy");
            if (logger == null) throw new ArgumentNullException("logger");

            this._customerServiceProxy = customerServiceProxy;
            this._logger = logger;
        }

        public async Task Consume(ConsumeContext<OrderCreated> context)
        {
            var message = context.Message;
            var customerIdentifier = CustomerIdentifier.FromCustomerId(message.CustomerId);

            _logger.Information("Received {eventName}: {@message}", typeof(OrderCreatedHandler).Name, message);

            var invoiceSettings = _customerServiceProxy.GetCustomerInvoiceCriteria(customerIdentifier);
            if (!invoiceSettings.DirectActivation)
            { 
                _logger.Information(
                    "Order {orderCode} not activated as the customer {customerNumber} does not have direct activation.",
                    message.OrderCode,
                    customerIdentifier.CustomerNumber);
                return;
            }

            await context.Publish(new ActivateOrder(message.OrderCode, ActivationMethod.Direct, "Customer has direct activation", 6, "System"));
            _logger.Information("Activate order command published for {orderCode}", message.OrderCode);
        }
    }
}