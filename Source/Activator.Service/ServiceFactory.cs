﻿using System.Collections.Generic;
using Billing.API.Common.Interface.Contracts.Services;
using Combell.BusinessLogic.WebService.Contracts;
using Core.Security;
using Core.Templating.API.Client;
using Intelligent.Shared.Core.Commands;
using Intelligent.Shared.Core.Queries;
using Intelligent.Shared.Messaging.MassTransit3;
using Intelligent.Shared.Messaging.MassTransit3.Serilog;
using Intelligent.Shared.Reflection;
using Intelligent.Shared.ServiceModel.Client;
using Intelligent.Shared.Services.Topshelf;
using MassTransit;
using Microsoft.Practices.Unity;
using Serilog;
using System.Configuration;
using Activator.Proxies;
using Activator.Services;
using Activator.Services.Activators;
using Activator.Services.Mapping;
using System.Runtime.Remoting.Messaging;
using System.Linq;

namespace Activator.Service
{
    public class ServiceFactory : UnityTopshelfServiceFactory
    {
        public override void ConfigureContainer(IUnityContainer container)
        {
            container.RegisterType<ILogger>(new ContainerControlledLifetimeManager(), new InjectionFactory(x => Log.Logger));
            container.RegisterInstance<IServiceInvoker<IBLWS_OrderForm>>(new ServiceInvoker<IBLWS_OrderForm>("BusinessLogicOrderForm"));
            container.RegisterInstance<IServiceInvoker<ICustomerService>>(new ServiceInvoker<ICustomerService>("Billing_Common_CustomerServiceConfiguration"));
            container.RegisterInstance<IServiceInvoker<IBLWS_UAC>>(new ServiceInvoker<IBLWS_UAC>("BusinessLogicUAC"));
            container.RegisterType<IOrderActivator, OrderActivator>(new ContainerControlledLifetimeManager());
            container.RegisterType<ICustomerServiceProxy, CustomerServiceProxy>(new ContainerControlledLifetimeManager());
            container.RegisterType<IProvisioningOrderServiceProxy, ProvisioningOrderServiceProxy>(new ContainerControlledLifetimeManager());
            container.RegisterType<IOrderContractToOrfOrderMapper, OrderContractToOrfOrderMapper>(new ContainerControlledLifetimeManager());
            container.RegisterType<IStoreApiProxy, StoreApiProxy>(new ContainerControlledLifetimeManager());
            container.RegisterType<ICustomerApiProxy, CustomerApiProxy>(new ContainerControlledLifetimeManager());
            container.RegisterType<IQueryHandler, QueryHandler>(new ContainerControlledLifetimeManager());
            container.RegisterType<ICommandHandler, CommandHandler>(new ContainerControlledLifetimeManager());
            container.RegisterInstance<ITokenProvider>(new TokenProvider(Scopes.Customer, "intelligent.orderdomain.activator", "activator"));
            container.RegisterType<IProvisioningOrderActivator, ProvisioningOrderActivator>(new ContainerControlledLifetimeManager());
            container.RegisterType<IPreRegistrationActivator, PreRegistrationActivator>(new ContainerControlledLifetimeManager());
            container.RegisterType<IServicePackageProvider, ServicePackageProvider>(new ContainerControlledLifetimeManager());
            RegisterOrderItemMappers(container);
            container.RegisterType<ITemplateClient, TemplateClient>(new ContainerControlledLifetimeManager());

            var busControl = BusControlBuilder.New(
                                ConfigurationManager.AppSettings["MassTransit_HostAddress"],
                                ConfigurationManager.AppSettings["MassTransit_UserName"],
                                ConfigurationManager.AppSettings["MassTransit_Password"]
                            )
                            .WithAddedBusConfiguration(cfg =>
                            {
                                cfg.ConfigurePublish(publishCfg => publishCfg.UseSendExecute(context =>
                                {
                                    var intelliProperties = (IEnumerable<KeyValuePair<string, string>>)CallContext.LogicalGetData("IntelliProperties");
                                    foreach (var intelliProperty in intelliProperties)
                                    {
                                        context.Headers.Set(intelliProperty.Key, intelliProperty.Value);
                                    }
                                }));
                            })
                            .AddConsumerEndpoint("orderdomain.activator.service", ep =>
                            {
                                ep.WithConcurrency(new ConcurrencySetting(1))
                                    .ForTypes(
                                        TypeFinder.FindTypesWhichImplement(
                                            typeof(ServiceFactory).Assembly,
                                            typeof(IConsumer<>)),
                                        type => container.Resolve(type));
                                ep.WithConsumerMw(endpointCfg =>
                                {
                                    endpointCfg.UseExecute(context =>
                                    {
                                        var convertedHeaders =
                                            context.Headers
                                                .GetAll()
                                                .Where(h => h.Key.StartsWith("Intelli"))
                                                .Select(h => new KeyValuePair<string, string>(h.Key, h.Value.ToString()));
                                        CallContext.LogicalSetData("IntelliProperties", convertedHeaders);
                                    });
                                });
                            })
                            .UseSeriLog()
                            .Build();
            container.RegisterBusControl(busControl);
        }

        private void RegisterOrderItemMappers(IUnityContainer container)
        {
            foreach (var type in TypeFinder.FindTypesWhichImplement<IOrderItemToProvisioningOrderMapper>(typeof(IOrderItemToProvisioningOrderMapper).Assembly))
            {
                container.RegisterType(typeof(IOrderItemToProvisioningOrderMapper), type, type.Name, new ContainerControlledLifetimeManager());
            }

            container.RegisterType<IEnumerable<IOrderItemToProvisioningOrderMapper>, IOrderItemToProvisioningOrderMapper[]>(new ContainerControlledLifetimeManager());

            container.RegisterType<IManualActivationMapper, ManualActivationMapper>(new ContainerControlledLifetimeManager());
        }
    }
}
