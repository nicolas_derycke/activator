﻿using Activator.Messages.Commands;
using MassTransit;
using Serilog;
using System;
using System.Threading.Tasks;
using Activator.Services.Activators;
using Intelligent.Core.Models;

namespace Activator.Service.CommandHandlers
{
    public class ActivateOrderHandler : IConsumer<ActivateOrder>
    {
        private readonly IOrderActivator _orderActivator;
        private readonly ILogger _logger;

        public ActivateOrderHandler(IOrderActivator orderActivator, ILogger logger)
        {
            if (orderActivator == null) throw new ArgumentNullException("orderActivator");
            if (logger == null) throw new ArgumentNullException("logger");

            this._orderActivator = orderActivator;
            this._logger = logger;
        }

        public async Task Consume(ConsumeContext<ActivateOrder> context)
        {
            var message = context.Message;

            if (string.IsNullOrWhiteSpace(message.OrderCode))
            {
                _logger.Information("Order not activated as no order code is present");
                return;
            }

            if (new OrderCode(message.OrderCode).Type != OrderCodeType.Store)
            {
                _logger.Information("Order {orderCode} not activated as this is an old order not created by the store api", message.OrderCode);
                return;
            }
            
            await _orderActivator.ActivateOrder(message.OrderCode, message.Method, message.ActivationReason, message.ActivationById, message.ActivationBy);
        }
    }
}