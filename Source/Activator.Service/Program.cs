﻿using Intelligent.Shared.Logging.Serilog;
using System.Collections.Generic;
using System.Runtime.Remoting.Messaging;

namespace Activator.Service
{
    class Program
    {
        static int Main(string[] args)
        {
            LogConfiguration.Initialize(config =>
                config.Enrich.WithProperties(() =>
                {
                    var headers = (IEnumerable<KeyValuePair<string, string>>)CallContext.LogicalGetData("IntelliProperties");
                    return headers;
                }));

            var factory = new ServiceFactory();
            return factory.Run<Service>();
        }
    }
}
