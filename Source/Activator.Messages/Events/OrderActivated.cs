﻿using System;
using System.Collections.Generic;

namespace Activator.Messages.Events
{
    public class OrderActivated
    {
        public string OrderCode { get; private set; }
        public ActivationMethod Method { get; set; }
        public string ActivationReason { get; private set; }
        public int ActivationById { get; private set; }
        public string ActivationBy { get; private set; }
        public IEnumerable<string> ProvisioningOrderCodes { get; private set; }
        public DateTime ActivatedOn { get; private set; }

        public OrderActivated(string orderCode, ActivationMethod method, string activationReason, int activationById, string activationBy, IEnumerable<string> provisioningOrderCodes, DateTime activatedOn)
        {
            OrderCode = orderCode;
            Method = method;
            ActivationReason = activationReason;
            ActivationById = activationById;
            ActivationBy = activationBy;
            ProvisioningOrderCodes = provisioningOrderCodes;
            ActivatedOn = activatedOn;
        }
    }
}