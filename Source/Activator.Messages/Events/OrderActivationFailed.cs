﻿using System;

namespace Activator.Messages.Events
{
    public class OrderActivationFailed
    {
        public string OrderCode { get; private set; }
        public DateTime ActivationFailedOn { get; private set; }

        public OrderActivationFailed(string orderCode, DateTime activationFailedOn)
        {
            this.OrderCode = orderCode;
            this.ActivationFailedOn = activationFailedOn;
        }
    }
}