﻿using System;

namespace Activator.Messages.Commands
{
    public class ActivateOrder
    {
        public string OrderCode { get; private set; }
        public ActivationMethod Method { get; set; }
        public string ActivationReason { get; private set; }
        public int ActivationById { get; private set; }
        public string ActivationBy { get; private set; }
        
        public ActivateOrder(string orderCode, ActivationMethod method, string activationReason, int activationById, string activationBy)
        {
            this.OrderCode = orderCode;
            this.Method = method;
            this.ActivationReason = activationReason;
            this.ActivationById = activationById;
            this.ActivationBy = activationBy;
        }
    }
}