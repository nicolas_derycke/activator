﻿namespace Activator.Messages
{
    public enum ActivationMethod
    {
        Direct,
        Manual,
        Payment,
        Free
    }
}