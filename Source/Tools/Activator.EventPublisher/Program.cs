﻿using Activator.Messages.Commands;
using Intelligent.Shared.Messaging.MassTransit3;
using Store.Messages.Events;
using Store.Messages.Payment;
using System.Configuration;

namespace Activator.EventPublisher
{
    class Program
    {
        static void Main(string[] args)
        {
            var busControl = BusControlBuilder.New(
                    ConfigurationManager.AppSettings["MassTransit_HostAddress"],
                    ConfigurationManager.AppSettings["MassTransit_UserName"],
                    ConfigurationManager.AppSettings["MassTransit_Password"]
                ).Build();

            //busControl.Publish(new OrderCreated("COM-20151120-131", CheckoutType.BankTransfer, 122829)).Wait();
            busControl.Publish(new ActivateOrder("COM-20160226-125", Messages.ActivationMethod.Manual, "Debugging", 88, "Zeno"));
        }
    }
}
